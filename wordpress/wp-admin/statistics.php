
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
		integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!--Bootstrap datetimepicker-->
	<link rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">
<?php
require_once __DIR__ . '/admin.php';
if ( ! current_user_can( 'read' ) ) {
	wp_die( __( 'Sorry, you are not allowed to see statistics for this site.' ) );
}

wp_enqueue_style( 'statistic', get_template_directory_uri() . '/styles/statistic.css' );

require_once ABSPATH . 'wp-admin/admin-header.php';
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<!-- Bootstrap: Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
		integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
		crossorigin="anonymous"></script>
	<script src="https://cdn.anychart.com/releases/v8/js//anychart-base.min.js"></script>
	<script src="https://cdn.anychart.com/releases/v8/js/anychart-exports.min.js"></script>
	<script src="https://cdn.anychart.com/releases/v8/js//anychart-ui.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.44/js/bootstrap-datetimepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.6.3/iframeResizer.contentWindow.min.js"></script>
<?php
wp_enqueue_script( 'statistic',  WP_SITEURL . WPINC . '/js/statistic.js');
?>


<div id="loader-wrapper" class="anychart-loader">
		<div class="rotating-cover">
			<div class="rotating-plane">
				<div class="chart-row"><span class="chart-col green"></span><span class="chart-col orange"></span><span
						class="chart-col red"></span></div>
			</div>
		</div>
	</div>
	<div class="wrapper">
		<div class="row">
			<div class="col-xs-12">
				<nav class="navbar navbar-default" id="container-timeline">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-date-time"
								aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="collapse navbar-collapse nav-date-time" id="nav-date-time">
							<ul class="nav navbar-nav">
								<li><a data-range="full">All</a></li>
								<li><a data-range="today" data-time="2016-12-31T23:59:00">Today</a></li>
								<li><a data-range="yesterday">Yesterday</a></li>
								<li><a data-range="week">Week</a></li>
								<li><a data-range="month">Month</a></li>
								<li><a data-range="quarter">Quarter</a></li>
							</ul>
						</div>
						<div class="datetimepicker-container">
							<div class="row">
								<div class='col-sm-5 col-xs-12'>
									<div class="form-group">
										<div class='input-group date' id='datetimepicker_start'>
											<input type='text' placeholder="Start Date Interval" class="form-control" />
											<span class="input-group-addon">
												<span class="ac ac-calendar"></span>
											</span>
										</div>
									</div>
								</div>
								<div class='col-sm-5 col-xs-12'>
									<div class="form-group">
										<div class='input-group date' id='datetimepicker_end'>
											<input type='text' placeholder="End Date Interval" class="form-control" />
											<span class="input-group-addon">
												<span class="ac ac-calendar"></span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-sm-2 col-xs-12 text-right">
									<button type="button" class="btn btn-info" id="set-date">Set Date</button>
								</div>
							</div>
						</div>

					</div><!-- /.container-fluid -->
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="chart_container" data-visible="line">
					<div class="chart line-container" id="chart1"></div>
					<i class="ac ac-chart-area toggle-chart icon-column" data-visible="line"></i>
					<i class="ac ac-chart-column2 toggle-chart icon-line" data-visible="column"></i>
					<div class="chart column-container" id="chart2"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="chart_container">
					<div class="chart" id="chart4" style="height: 1000px;"></div>
				</div>
			</div>
		</div>

</div>

<?php
require_once ABSPATH . 'wp-admin/admin-footer.php';
