<?php

use Carbon\Carbon;

class WP_REST_Statistics_Controller extends WP_REST_Controller
{
    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var string
     */
    protected $rest_base;

    /**
     * @return void
     */
	public function __construct()
    {
		$this->namespace = 'statistics/v1';
		$this->rest_base = 'visits';
	}

    /**
     * @return void
     */
	public function register_routes()
    {
		register_rest_route(
			$this->namespace,
			'/' . $this->rest_base,
			[
				[
					'methods'             => WP_REST_Server::READABLE,
					'callback'            => [ $this, 'get_items' ],
					'args'                => [],
					'permission_callback' => [ $this, 'get_item_permissions_check' ],
				],
			]
		);
	}

    /**
     * @param WP_REST_Request $request
     * @return WP_REST_Response
     */
    public function get_items($request): WP_REST_Response
    {
        $filters = $this->getFiltersFromRequest($request);

        $results = [
            'data' => [
                'statistics' => $this->getStatisticsByFilters($filters),
            ],
            'meta' => [],
        ];

        return rest_ensure_response( $results );
    }

    /**
     * @param WP_REST_Request $request
     * @return bool
     */
    public function get_item_permissions_check($request): bool
    {
        return true;
    }

    /**
     * @param WP_REST_Request $request
     * @return array
     */
    private function getFiltersFromRequest(WP_REST_Request $request): array
    {
        $dateFromParam = $request->get_param('dateFrom');
        $dateToParam = $request->get_param('dateTo');

        $dateFromObject = $dateFromParam
            ? Carbon::parse($dateFromParam)
            : Carbon::now(TIMEZONE_EUROPE_KIEV)->startOfDay();
        $dateToObject = $dateToParam
            ? Carbon::parse($dateToParam)
            : Carbon::now(TIMEZONE_EUROPE_KIEV)->addDay()->endOfDay();

        return [
            'dateFrom' => $dateFromObject->format('Y-m-d'),
            'dateTo' => $dateToObject->format('Y-m-d'),
            'page' => $request->get_param('page'),
        ];
    }

    /**
     * @param array $filters
     * @return array
     */
    private function getStatisticsByFilters(array $filters): array
    {
        global $wpdb;

        $summaryVisitsResult = $wpdb->get_results(
            $this->getSummaryVisitsQuery($filters),
            ARRAY_A
        );

        $byPageVisitsResult = $wpdb->get_results(
            $this->getByPageVisitsQuery($filters),
            ARRAY_A
        );

        $byDaysVisitsResult = $wpdb->get_results(
            $this->getByDaysVisitsQuery($filters),
            ARRAY_A
        );

        return [
            'summaryVisits' => $summaryVisitsResult[0]['sum_count'] ?? null,
            'byPageVisits' => $byPageVisitsResult,
            'byDaysVisits' => $byDaysVisitsResult,
        ];
    }

    /**
     * @param array $filters
     * @return string
     */
    private function getSummaryVisitsQuery(array $filters): string
    {
        return "
            SELECT
                SUM(count) as sum_count
            FROM `ip_visits` WHERE
                `day_of_visit` BETWEEN '{$filters['dateFrom']}' AND '{$filters['dateTo']}'
        ";
    }

    /**
     * @param array $filters
     * @return string
     */
    private function getByPageVisitsQuery(array $filters): string
    {
        return "
            SELECT
                `url`,
                SUM(count) AS visits
            FROM `ip_visits`
                WHERE `day_of_visit` BETWEEN '{$filters['dateFrom']}' AND '{$filters['dateTo']}'
                GROUP BY `url`;
        ";
    }

    /**
     * @param array $filters
     * @return string
     */
    private function getByDaysVisitsQuery(array $filters): string
    {
        return "
            SELECT
                `day_of_visit`,
                SUM(count) AS visits
            FROM `ip_visits`
                WHERE `day_of_visit` BETWEEN '{$filters['dateFrom']}' AND '{$filters['dateTo']}'
                GROUP BY `day_of_visit`;
        ";
    }
}
