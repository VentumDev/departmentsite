(function () {
    anychart.format.inputDateTimeFormat('MM/dd/yyyy');
    var $datetimepicker_start = $('#datetimepicker_start');
    var $datetimepicker_end = $('#datetimepicker_end');
    var $nav = $('#nav-date-time');
    var $button_set_date = $('button#set-date');
    var $toggle_chart = $('.toggle-chart');
    var today = new Date();

    console.log('today', $nav.find('[data-range="today"]').data('time'), (new Date()).toLocaleString());
    today = new Date(today);
    var week = new Date(today.getFullYear(), today.getMonth(), (today.getDate() - 6), 23, 59).getTime();
    var tickets = [];
    var defaultDateStart;
    var defaultDateEnd;
    var minDate;
    var maxDate;
    var from;
    var to;
    var chartColumn = anychart.column();

    //Draw Tickets Timeline Line Chart
    async function lineDiagram(chartData) {
        let chartSeriesData = chartData.map(data => [Date.parse(data.day_of_visit), data.visits]);
        var chart = anychart.line();
        var chartCall = chart.line(chartSeriesData);
        chartCall
            .markers('circle')
            .zIndex(43)
            .clip(false);

        chart.xScale('date-time');

        var chartXScale = chart.xScale();
        chartXScale
            .minimumGap(0)
            .maximumGap(0);

        var chartSeriesCall = chart.getSeries(0);
        chartSeriesCall.name('Кількість відвідувачів');

        var chartTitle = chart.title();
        chartTitle
            .enabled(true)
            .text('Tickets Timeline')
            .textOverflow('...')
            .align('left');

        var chartLegend = chart.legend();
        chartLegend
            .enabled(true)
            .align('right');

        var chartTooltip = chart.tooltip();
        chartTooltip
            .displayMode('union')
            .titleFormat(function () {
                return anychart.format.dateTime(this.points[0].x, 'MM/dd/yyyy');
            });

        var chartXAxisLabels = chart.xAxis().labels();
        chartXAxisLabels
            .format(function () {
                return anychart.format.dateTime(this.tickValue, 'MM/dd/yyyy');
            });
        document.getElementById('chart1').innerHTML = '';
        chart.container('chart1').draw();
    }

    //Draw Types of Problems
    async function columnDiagram(chartData) {
        let pagesByDate = chartData.filter(item => !item.url.includes('?'));
        var chartSeriesData = pagesByDate.map(data => [data.url.substring(0, 50), data.visits]);
        var chart = anychart.bar(chartSeriesData);

        var chartTitle = chart.title();
        chartTitle
            .enabled(true)
            .text('Types of Problems')
            .textOverflow('...')
            .align('left');

        var chartSeries = chart.getSeries(0);
        chartSeries.name('Amount of Tickets');

        document.getElementById('chart4').innerHTML = '';
        chart.container('chart4').draw();
    }

    //Initialization DateTimePicker and default dates
    function initDateTime() {
        $datetimepicker_start.datetimepicker();
        $datetimepicker_end.datetimepicker();

        $datetimepicker_start.on("dp.change", function (e) {
            $(this).datetimepicker('hide');
            $nav.find('li').removeClass('active');
        });

        $datetimepicker_end.on("dp.change", function (e) {
            $(this).datetimepicker('hide');
            $nav.find('li').removeClass('active');
        });

        defaultDateStart = anychart.format.dateTime(week, "MM/dd/yyyy HH:mm", new Date().getTimezoneOffset());
        defaultDateEnd = anychart.format.dateTime(today.getTime(), "MM/dd/yyyy HH:mm", new Date().getTimezoneOffset());
        console.log("default", defaultDateStart, defaultDateEnd)
        $datetimepicker_start.data('DateTimePicker').date(defaultDateStart);
        $datetimepicker_end.data('DateTimePicker').date(defaultDateEnd);

        initToggle();
    }

    //Navigation bar and timerange
    $nav.on('click', 'a', function () {
        var range = $(this).data('range');

        switch (range) {
            case 'today': {
                from = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 1).getTime() / 1000;
                to = today.getTime() / 1000;
                break;
            }
            case 'yesterday': {
                from = new Date(today.getFullYear(), today.getMonth(), (today.getDate() - 1), 0, 0).getTime() / 1000;
                to = today.getTime() / 1000;
                break;
            }
            case 'week': {
                from = new Date(today.getFullYear(), today.getMonth(), (today.getDate() - 6), 23, 59).getTime() / 1000;
                to = today.getTime() / 1000;
                break;
            }
            case 'month': {
                from = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate(), 23, 59).getTime() / 1000;
                to = today.getTime() / 1000;
                break;
            }
            case 'quarter': {
                from = new Date(today.getFullYear(), today.getMonth() - 3, today.getDate(), 23, 59).getTime() / 1000;
                to = today.getTime() / 1000;
                chartColumn.xScroller().enabled(true);
                chartColumn.xZoom().setTo(0, 0.2);
                break;
            }
            case 'full': {
                from = minDate;
                to = today.getTime() / 1000;
                chartColumn.xScroller().enabled(true);
                chartColumn.xZoom().setTo(0, 0.15);
            }
        }

        $datetimepicker_start.data('DateTimePicker').date(anychart.format.dateTime(from * 1000, "MM/dd/yyyy HH:mm", new Date().getTimezoneOffset()));
        $datetimepicker_end.data('DateTimePicker').date(anychart.format.dateTime(to * 1000, "MM/dd/yyyy HH:mm", new Date().getTimezoneOffset()));

        filterByDate(tickets, from, to);

        $(this).parents($nav).find('li').removeClass('active');
        $(this).closest('li').addClass('active');
    });

    // Button "Set Date" pressed
    $button_set_date.on('click', ShowDataByRange);

    async function ShowDataByRange() {
      from = new Date($datetimepicker_start.data('DateTimePicker').date().toDate()).getTime() / 1000;
      to = new Date($datetimepicker_end.data('DateTimePicker').date().toDate()).getTime() / 1000;
      filterByDate(tickets, from, to);
    }


    //Filtering data by date
    async function filterByDate(data, dateStart, dateEnd) {
      dateStart = typeof dateStart !== 'undefined' ? dateStart : 0;
      console.log("sdfajkdfjsad    ", dateStart, dateEnd);
        let dateStartString = (new Date(dateStart * 1000 )).toLocaleDateString();
        let dateEndString = (new Date(dateEnd * 1000 )).toLocaleDateString();
        let remoteData = await GetDataByDate(dateStartString, dateEndString);

        lineDiagram(remoteData.byDaysVisits);
        columnDiagram(remoteData.byPageVisits);
    }

    async function GetDataByDate(dateStart = '', dateEnd = '') {
      console.log('st en', dateStart, dateEnd);
      let url = 'http://192.168.43.165/portal/wordpress/wp-json/statistics/v1/visits?';
      let fetchData = await fetch(url + new URLSearchParams({
        dateFrom: dateStart,
        dateTo: dateEnd,
      }).toString());

      console.log(url + new URLSearchParams({
        dateFrom: dateStart,
        dateTo: dateEnd,
      }).toString());

      return (await fetchData.json()).data.statistics;
    } 

    function hidePreloader() {
        $('#loader-wrapper').fadeOut('slow');
    }

    function initToggle() {
        $('.chart_container').each(function () {
            var $parent = $(this);

            if ($(this).attr('data-visible') === 'line') {
                $parent.find('.column-container').hide();
                $parent.find('.line-container').show();

                $parent.find('.icon-line').show();
                $parent.find('.icon-column').hide();

            } else if ($(this).attr('data-visible') === 'column') {
                $parent.find('.line-container').hide();
                $parent.find('.column-container').show();

                $parent.find('.icon-line').hide();
                $parent.find('.icon-column').show();

                $(this).addClass('active')
            }
        });
    }

    $(window).on('load', function () {
        hidePreloader();
    });

    $toggle_chart.on('click', function () {
        var $parent = $(this).closest('.chart_container');

        if ($parent.attr('data-visible') === 'column') {
            $parent.find('.column-container').fadeOut('fast', function () {
                $parent.find('.line-container').fadeIn('slow');

                $parent.find('.icon-line').show();
                $parent.find('.icon-column').hide();

                $parent.attr('data-visible', 'line');
            });

        } else if ($parent.attr('data-visible') === 'line') {
            $parent.find('.line-container').fadeOut('fast', function () {
                $parent.find('.column-container').fadeIn('slow');
                $parent.attr('data-visible', 'column');

                $parent.find('.icon-line').hide();
                $parent.find('.icon-column').show();
            });
        }
    });

    anychart.onDocumentReady(function () {
        initDateTime();
        ShowDataByRange()
    });
})();