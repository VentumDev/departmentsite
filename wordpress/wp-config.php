<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'site' );

/** MySQL database username */
define( 'DB_USER', 'site' );

/** MySQL database password */
define( 'DB_PASSWORD', '1aA!11111111' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4>IfQi##wx{UCO(D6cgA6C$79CLFfv#g{ +DkA3p8>;Go(vh)UK2t>[iuI@_^/F.' );
define( 'SECURE_AUTH_KEY',  '_As~*M1:5V(o._?):~4-{9]aj$ltqz)$K4>1iSOQ&Vl~0] ;GD dvz_?0ZT=Qrpm' );
define( 'LOGGED_IN_KEY',    'Fegv,_Us|6$Y-k>^6FJPLN{8;|Dl2uhA3-41e;f=rWM]dY_$E}{d)%@D6r$J-##3' );
define( 'NONCE_KEY',        'KR8U&dJTWGB2-h+dyha#Iy-CtAQvT:D?3L!lL:uFZinYcT{W3?>=TG0wI&_O<2/X' );
define( 'AUTH_SALT',        'T&}*F$@+P)J<55*G>vnx??(%!tRWVIJ<`LMXtvsy2diAS0;wGLk8`2^/)OU=.&[x' );
define( 'SECURE_AUTH_SALT', 'xwqke=pLHd5M9KG9+O!x{%zpM_f^?0;/Nkj.^qnCW<d&N^SX_-;$k0?vRA#Pqk19' );
define( 'LOGGED_IN_SALT',   './Ojd5+)w}9#X?4h5)K$`eI%v5@c3.s0NfFD4cwhef/vp)d-xv6C=<.3U 1z#ZV0' );
define( 'NONCE_SALT',       '|V}O?G~C3GWR<x!]%IXgBF]_;Ngw*8WW5}-H@`QT)_&/]N)1dcA|>BV;^[)Of!-@' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

const WP_HOME = 'http://192.168.43.165/portal/wordpress/';
const WP_SITEURL = 'http://192.168.43.165/portal/wordpress/';
const TIMEZONE_EUROPE_KIEV = 'Europe/Kiev';

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
