<?php

function saveVisitedUrl()
{
    date_default_timezone_set(TIMEZONE_EUROPE_KIEV);

    insertOrIncrement('ip_visits', [
        'ip' => $_SERVER['REMOTE_ADDR'],
        'url' => parse_url(
            urldecode($_SERVER['REQUEST_URI']),
            PHP_URL_PATH
        ),
        'day_of_visit' => date('Y-m-d'),
    ], [
        'count' => 1,
    ]);
}

/**
 * @param string $table
 * @param array $conditions
 * @param array $additionalData
 * @return void
 */
function insertOrIncrement(string $table, array $conditions, array $additionalData = []): void
{
    $row = getRow($table, $conditions);

    if ($row) {
        incrementRow($table, $row, 'count');
        return;
    }

    insertRow($table, $conditions, $additionalData);
}

/**
 * @param string $table
 * @param array $conditions
 * @return array|null
 */
function getRow(string $table, array $conditions): ?array
{
    global $wpdb;

    $statements = [];

    foreach ($conditions as $column => $value) {
        $statements[] = "{$column} = \"{$value}\"";
    }

    $plainConditions = implode(' AND ', $statements);

    return $wpdb->get_row("Select * from {$table} WHERE {$plainConditions}", ARRAY_A);
}

/**
 * @param string $table
 * @param array $conditions
 * @param array $additionalData
 * @return bool|int
 */
function insertRow(string $table, array $conditions, array $additionalData = [])
{
    global $wpdb;

    return $wpdb->insert($table, array_merge($conditions, $additionalData));
}

/**
 * @param string $table
 * @param array $row
 * @param string $column
 * @param int $value
 * @return bool|int
 */
function incrementRow(string $table, array $row, string $column, int $value = 1)
{
    global $wpdb;

    return $wpdb->update($table, [
        $column => $row['count'] + $value,
    ], [
        'id' => $row['id'],
    ]);
}

saveVisitedUrl();
