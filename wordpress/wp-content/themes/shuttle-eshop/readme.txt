=== Shuttle eShop ===
Contributors: shuttlethemes
Version: 1.0.7
Requires at least: 5.0
Tested up to: 5.6
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns, three-columns, right-sidebar, left-sidebar, custom-header, custom-menu, full-width-template, theme-options, threaded-comments, editor-style, featured-images, featured-image-header, post-formats, sticky-post, translation-ready, flexible-header, custom-background, grid-layout, footer-widgets, blog, e-commerce, education, entertainment, news, photography, portfolio


== Description ==

Shuttle eShop is the awesome free version of Shuttle Pro. It is really good for professionals. If you want to make a business, for big or small this theme is good for you. So if it is a restaurant, sport, medical, startup, corporate, business, ecommerce, portfolio, freelancers or any type of online agency or firm you will want to use this cool design. It had a multi-purpose design with widget areas in footer, so now even easy to make blog / news website which looks really clean and professional. The theme is responsive, WPML, Polylang, Retina ready, SEO friendly, and is a super design. Shuttle is fast and lightweight and can be used for any type of website, fully compatible with eCommerce plugins like WooCommerce an JigoShop. Whether you are building a website for yourself, your business or are a freelancer building websites for your cliente, Shuttle is the perfect choice. Plus if works with all the major page builders such as Elementor, Beaver Builder, Visual Composer, Divi, SiteOrigin and so much more!

For support for Shuttle eShop please go to https://wordpress.org/support/theme/shuttle-eshop.


== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Shuttle eShop in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.


== Copyright ==

Shuttle eShop WordPress Theme is child theme of Shuttle WordPress Theme, Copyright 2018 Shuttle Themes
Shuttle WordPress Theme is distributed under the terms of the GNU GPL.

Shuttle eShop WordPress Theme, Copyright 2021 Shuttle Themes
Shuttle eShop is distributed under the terms of the GNU GPL

Shuttle eShop is a child theme of Shuttle and as such uses the same opensource projects as its parent theme.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Shuttle eShop uses the following third-party resources:

https://pxhere.com/en/photo/1430495

== Changelog ==

= 1.0.7 =
- Copyright date fixed to be 2021.

= 1.0.6 =
- Improved accessibility for links.

= 1.0.5 =
- Version bump for updated release.

= 1.0.4 =
- Readme updated to include "Stable tag" tag.

= 1.0.3 =
- Version bump of reviewer update.

= 1.0.2 =
- Minor styling updates.

= 1.0.1 =
- Minor styling updates.
- Screenshot updated.

= 1.0.0 =
- Public release.
