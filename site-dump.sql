-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Лис 09 2021 р., 15:24
-- Версія сервера: 10.4.20-MariaDB
-- Версія PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `site`
--

-- --------------------------------------------------------

--
-- Структура таблиці `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Коментатор WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-07-23 20:08:05', '2021-07-23 17:08:05', 'Привіт! Це коментар.\nЩоб почати модерувати, редагувати і видаляти коментарі, перейдіть в розділ Коментарів у Майстерні.\nАватари авторів коментарів завантажуються з сервісу<a href=\"https://uk.gravatar.com\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/departmentsite/wordpress', 'yes'),
(2, 'home', 'http://localhost/departmentsite/wordpress', 'yes'),
(3, 'blogname', '', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'poanan96@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'd.m.Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:94:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:21:\"megamenu/megamenu.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'shuttle', 'yes'),
(41, 'stylesheet', 'shuttle-eshop', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '1', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'posts', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(78, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', 'Europe/Kiev', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '0', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '21', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1642612080', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'initial_db_version', '49752', 'yes'),
(99, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(100, 'fresh_site', '0', 'yes'),
(101, 'WPLANG', 'uk', 'yes'),
(102, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'sidebars_widgets', 'a:12:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"footer-w1\";a:0:{}s:9:\"footer-w2\";a:0:{}s:9:\"footer-w3\";a:0:{}s:9:\"footer-w4\";a:0:{}s:9:\"footer-w5\";a:0:{}s:9:\"footer-w6\";a:0:{}s:13:\"sub-footer-w1\";a:0:{}s:13:\"sub-footer-w2\";a:0:{}s:9:\"mega-menu\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(108, 'cron', 'a:8:{i:1632467287;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1632503286;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1632503287;a:4:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1632503302;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1632503303;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1632503306;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1632589686;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(109, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(117, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(119, 'recovery_keys', 'a:0:{}', 'yes'),
(120, 'theme_mods_twentytwentyone', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1628437143;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}}}s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}}', 'yes'),
(121, 'https_detection_errors', 'a:1:{s:23:\"ssl_verification_failed\";a:1:{i:0;s:38:\"Невдала перевірка SSL.\";}}', 'yes'),
(141, 'can_compress_scripts', '1', 'no'),
(154, 'finished_updating_comment_type', '1', 'yes'),
(169, '_transient_health-check-site-status-result', '{\"good\":\"13\",\"recommended\":\"6\",\"critical\":\"1\"}', 'yes'),
(191, 'wpzoom_misc_feedburner', '', 'yes'),
(192, 'wpzoom_tel_enable', 'on', 'yes'),
(193, 'wpzoom_tel_caption', 'Edit this in WPZOOM Theme Options ', 'yes'),
(194, 'wpzoom_tel_text', '800-123-456', 'yes'),
(195, 'wpzoom_theme_style', 'Default', 'yes'),
(196, 'wpzoom_display_posts_columns', 'One', 'yes'),
(197, 'wpzoom_display_with_thumbs', '4', 'yes'),
(198, 'wpzoom_excerpt_length', '30', 'yes'),
(199, 'wpzoom_display_date', 'on', 'yes'),
(200, 'wpzoom_display_category', 'on', 'yes'),
(201, 'wpzoom_post_date', 'on', 'yes'),
(202, 'wpzoom_post_category', 'off', 'yes'),
(203, 'wpzoom_post_author', 'on', 'yes'),
(204, 'wpzoom_post_tags', 'on', 'yes'),
(205, 'wpzoom_post_share', 'on', 'yes'),
(206, 'wpzoom_post_comments', 'on', 'yes'),
(207, 'wpzoom_page_share', 'on', 'yes'),
(208, 'wpzoom_page_comments', 'on', 'yes'),
(209, 'wpzoom_featured_posts_show', 'on', 'yes'),
(210, 'wpzoom_featured_type', 'Featured Posts', 'yes'),
(211, 'wpzoom_slideshow_auto', 'off', 'yes'),
(212, 'wpzoom_slideshow_speed', '3000', 'yes'),
(213, 'wpzoom_slideshow_posts', '5', 'yes'),
(214, 'wpzoom_slider_date', 'on', 'yes'),
(215, 'wpzoom_slider_excerpt', 'on', 'yes'),
(216, 'wpzoom_slider_button', 'on', 'yes'),
(217, 'wpzoom_header_code', '', 'yes'),
(218, 'wpzoom_footer_code', '', 'yes'),
(219, 'wpzoom_meta_generator', 'on', 'yes'),
(220, 'wpzoom_framework_fonts_preview', 'on', 'yes'),
(221, 'wpzoom_framework_update_enable', 'on', 'yes'),
(222, 'wpzoom_framework_update_notification_enable', 'on', 'yes'),
(223, 'wpzoom_framework_theme_update_notification_enable', 'on', 'yes'),
(224, 'wpzoom_framework_newthemes_enable', 'on', 'yes'),
(225, 'wpzoom_framework_shortcodes_enable', 'on', 'yes'),
(226, 'wpzoom_framework_wzslider_enable', 'on', 'yes'),
(227, 'wpzoom_misc_import', '', 'yes'),
(228, 'wpzoom_misc_load_default_widgets', '', 'yes'),
(229, 'wpzoom_misc_import_widgets', '', 'yes'),
(230, 'recovery_mode_email_last_sent', '1628414953', 'yes'),
(233, 'recently_activated', 'a:2:{s:33:\"aapside-master/appside-master.php\";i:1628434053;s:19:\"akismet/akismet.php\";i:1628429705;}', 'yes'),
(238, 'widget_appside_about_us', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(239, 'widget_appside_popular_posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(240, 'widget_appside_contact_info', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(242, 'widget_akismet_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(244, 'appside_theme_options', 'a:86:{s:16:\"preloader_enable\";b:1;s:18:\"preloader_bg_color\";s:7:\"#ffffff\";s:15:\"back_top_enable\";b:1;s:13:\"back_top_icon\";s:14:\"fa fa-angle-up\";s:24:\"global_navbar_build_type\";s:7:\"default\";s:19:\"global_header_style\";s:7:\"default\";s:27:\"global_header_builder_style\";s:0:\"\";s:24:\"global_footer_build_type\";s:7:\"default\";s:27:\"global_footer_builder_style\";s:0:\"\";s:15:\"header_one_logo\";s:0:\"\";s:21:\"header_one_navbar_btn\";b:1;s:26:\"header_one_navbar_btn_text\";s:8:\"Download\";s:25:\"header_one_navbar_btn_url\";s:1:\"#\";s:15:\"header_two_logo\";s:0:\"\";s:21:\"header_two_navbar_btn\";b:1;s:26:\"header_two_navbar_btn_text\";s:8:\"Download\";s:25:\"header_two_navbar_btn_url\";s:1:\"#\";s:17:\"header_three_logo\";s:0:\"\";s:23:\"header_three_navbar_btn\";b:1;s:28:\"header_three_navbar_btn_text\";s:5:\"Login\";s:27:\"header_three_navbar_btn_url\";s:1:\"#\";s:27:\"header_three_navbar_btn_two\";b:1;s:32:\"header_three_navbar_btn_two_text\";s:6:\"Signup\";s:31:\"header_three_navbar_btn_two_url\";s:1:\"#\";s:16:\"header_four_logo\";s:0:\"\";s:22:\"header_four_navbar_btn\";b:1;s:27:\"header_four_navbar_btn_text\";s:8:\"Download\";s:26:\"header_four_navbar_btn_url\";s:1:\"#\";s:17:\"breadcrumb_enable\";b:1;s:13:\"breadcrumb_bg\";a:3:{s:15:\"background-size\";s:5:\"cover\";s:19:\"background-position\";s:13:\"center bottom\";s:17:\"background-repeat\";s:9:\"no-repeat\";}s:14:\"footer_spacing\";b:1;s:18:\"footer_top_spacing\";i:100;s:21:\"footer_bottom_spacing\";i:65;s:22:\"copyright_area_spacing\";b:1;s:14:\"copyright_text\";s:0:\"\";s:26:\"copyright_area_top_spacing\";i:20;s:29:\"copyright_area_bottom_spacing\";i:20;s:19:\"blog_post_posted_by\";b:1;s:19:\"blog_post_posted_on\";b:1;s:22:\"blog_post_readmore_btn\";b:1;s:27:\"blog_post_readmore_btn_text\";s:9:\"Read More\";s:22:\"blog_post_excerpt_more\";s:0:\"\";s:24:\"blog_post_excerpt_length\";s:0:\"\";s:26:\"blog_single_post_posted_by\";b:1;s:26:\"blog_single_post_posted_on\";b:1;s:32:\"blog_single_post_posted_category\";b:1;s:27:\"blog_single_post_posted_tag\";b:1;s:29:\"blog_single_post_posted_share\";b:1;s:12:\"404_bg_color\";s:7:\"#ffffff\";s:9:\"404_title\";s:0:\"\";s:12:\"404_subtitle\";s:0:\"\";s:13:\"404_paragraph\";s:0:\"\";s:15:\"404_button_text\";s:0:\"\";s:15:\"404_spacing_top\";i:120;s:18:\"404_spacing_bottom\";i:120;s:11:\"blog_layout\";s:13:\"right-sidebar\";s:13:\"blog_bg_color\";s:7:\"#ffffff\";s:16:\"blog_spacing_top\";i:120;s:19:\"blog_spacing_bottom\";i:120;s:18:\"blog_single_layout\";s:13:\"right-sidebar\";s:20:\"blog_single_bg_color\";s:7:\"#ffffff\";s:23:\"blog_single_spacing_top\";i:120;s:26:\"blog_single_spacing_bottom\";i:120;s:14:\"archive_layout\";s:13:\"right-sidebar\";s:16:\"archive_bg_color\";s:7:\"#ffffff\";s:19:\"archive_spacing_top\";i:120;s:22:\"archive_spacing_bottom\";i:120;s:13:\"search_layout\";s:13:\"right-sidebar\";s:15:\"search_bg_color\";s:7:\"#ffffff\";s:18:\"search_spacing_top\";i:120;s:21:\"search_spacing_bottom\";i:120;s:18:\"portfolio_bg_color\";s:7:\"#ffffff\";s:21:\"portfolio_spacing_top\";i:120;s:24:\"portfolio_spacing_bottom\";i:120;s:23:\"portfolio_sidebar_title\";s:11:\"Information\";s:25:\"portfolio_sidebar_clients\";s:6:\"Client\";s:25:\"portfolio_sidebar_company\";s:7:\"Company\";s:25:\"portfolio_sidebar_website\";s:7:\"Website\";s:26:\"portfolio_sidebar_category\";s:8:\"Category\";s:28:\"portfolio_sidebar_start_date\";s:10:\"Start Date\";s:26:\"portfolio_sidebar_end_date\";s:8:\"End Date\";s:10:\"_body_font\";a:4:{s:11:\"font-family\";s:7:\"Poppins\";s:9:\"font-size\";s:2:\"16\";s:4:\"unit\";s:1:\"%\";s:4:\"type\";s:6:\"google\";}s:17:\"body_font_variant\";a:2:{i:0;s:3:\"400\";i:1;s:3:\"700\";}s:19:\"heading_font_enable\";b:0;s:12:\"heading_font\";a:2:{s:11:\"font-family\";s:7:\"Poppins\";s:4:\"type\";s:6:\"google\";}s:20:\"heading_font_variant\";a:5:{i:0;s:3:\"400\";i:1;s:3:\"500\";i:2;s:3:\"600\";i:3;s:3:\"700\";i:4;s:3:\"800\";}}', 'yes'),
(245, 'appside_customize_options', 'a:37:{s:10:\"main_color\";s:7:\"#500ade\";s:15:\"secondary_color\";s:7:\"#111d5c\";s:13:\"heading_color\";s:7:\"#1c144e\";s:15:\"paragraph_color\";s:7:\"#878a95\";s:26:\"header_01_nav_bar_bg_color\";s:4:\"#fff\";s:23:\"header_01_nav_bar_color\";s:7:\"#878a95\";s:27:\"header_01_dropdown_bg_color\";s:7:\"#ffffff\";s:24:\"header_01_dropdown_color\";s:7:\"#878a95\";s:26:\"header_02_nav_bar_bg_color\";s:11:\"transparent\";s:23:\"header_02_nav_bar_color\";s:24:\"rgba(255, 255, 255, 0.8)\";s:27:\"header_02_dropdown_bg_color\";s:7:\"#ffffff\";s:24:\"header_02_dropdown_color\";s:7:\"#878a95\";s:26:\"header_03_nav_bar_bg_color\";s:11:\"transparent\";s:23:\"header_03_nav_bar_color\";s:24:\"rgba(255, 255, 255, 0.8)\";s:27:\"header_03_dropdown_bg_color\";s:7:\"#ffffff\";s:24:\"header_03_dropdown_color\";s:7:\"#878a95\";s:30:\"header_03_btn_one_border_color\";s:20:\"rgba(255,255,255,.8)\";s:23:\"header_03_btn_one_color\";s:20:\"rgba(255,255,255,.8)\";s:36:\"header_03_btn_one_hover_border_color\";s:7:\"#500ade\";s:29:\"header_03_btn_one_hover_color\";s:4:\"#fff\";s:32:\"header_03_btn_one_hover_bg_color\";s:7:\"#500ade\";s:34:\"header_03_btn_two_background_color\";s:7:\"#500ade\";s:23:\"header_03_btn_two_color\";s:20:\"rgba(255,255,255,.8)\";s:26:\"header_04_nav_bar_bg_color\";s:4:\"#fff\";s:23:\"header_04_nav_bar_color\";s:7:\"#878a95\";s:27:\"header_04_dropdown_bg_color\";s:7:\"#ffffff\";s:24:\"header_04_dropdown_color\";s:7:\"#878a95\";s:30:\"header_04_btn_background_color\";s:7:\"#500ade\";s:19:\"header_04_btn_color\";s:20:\"rgba(255,255,255,.8)\";s:27:\"sidebar_widget_border_color\";s:7:\"#fafafa\";s:26:\"sidebar_widget_title_color\";s:7:\"#242424\";s:25:\"sidebar_widget_text_color\";s:7:\"#777777\";s:20:\"footer_area_bg_color\";s:7:\"#0d2753\";s:25:\"footer_widget_title_color\";s:7:\"#ffffff\";s:24:\"footer_widget_text_color\";s:24:\"rgba(255, 255, 255, 0.7)\";s:31:\"copyright_area_border_top_color\";s:24:\"rgba(255, 255, 255, 0.2)\";s:25:\"copyright_area_text_color\";s:24:\"rgba(255, 255, 255, 0.6)\";}', 'yes'),
(248, 'theme_mods_theme', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1628434165;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(249, 'current_theme', 'Shuttle eShop', 'yes'),
(250, 'theme_switched', '', 'yes'),
(251, 'theme_switched_via_customizer', '', 'yes'),
(252, 'customize_stashed_theme_mods', 'a:0:{}', 'no'),
(273, 'widget_maxmegamenu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(274, 'megamenu_version', '2.9.4', 'yes'),
(275, 'megamenu_initial_version', '2.9.4', 'yes'),
(276, 'megamenu_multisite_share_themes', 'false', 'yes'),
(277, 'megamenu_settings', 'a:4:{s:6:\"prefix\";s:8:\"disabled\";s:12:\"descriptions\";s:7:\"enabled\";s:12:\"second_click\";s:2:\"go\";s:7:\"primary\";a:7:{s:7:\"enabled\";s:1:\"1\";s:5:\"event\";s:5:\"hover\";s:6:\"effect\";s:7:\"fade_up\";s:12:\"effect_speed\";s:3:\"200\";s:13:\"effect_mobile\";s:8:\"disabled\";s:19:\"effect_speed_mobile\";s:3:\"200\";s:5:\"theme\";s:7:\"default\";}}', 'yes'),
(280, '_transient_timeout_megamenu_css_version', '4782035128', 'no'),
(281, '_transient_megamenu_css_version', '2.9.4', 'no'),
(282, '_transient_timeout_megamenu_css_last_updated', '4782035128', 'no'),
(283, '_transient_megamenu_css_last_updated', '1628435128', 'no'),
(284, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(285, 'megamenu_themes', 'a:1:{s:7:\"default\";a:221:{s:5:\"title\";s:7:\"Default\";s:8:\"arrow_up\";s:9:\"dash-f142\";s:10:\"arrow_down\";s:9:\"dash-f140\";s:10:\"arrow_left\";s:9:\"dash-f141\";s:11:\"arrow_right\";s:9:\"dash-f139\";s:11:\"line_height\";s:3:\"1.7\";s:7:\"z_index\";s:3:\"999\";s:17:\"shadow_horizontal\";s:3:\"0px\";s:15:\"shadow_vertical\";s:3:\"0px\";s:11:\"shadow_blur\";s:3:\"5px\";s:13:\"shadow_spread\";s:3:\"0px\";s:12:\"shadow_color\";s:18:\"rgba(0, 0, 0, 0.1)\";s:21:\"menu_item_link_height\";s:4:\"40px\";s:25:\"container_background_from\";s:4:\"#222\";s:23:\"container_background_to\";s:4:\"#222\";s:21:\"container_padding_top\";s:3:\"0px\";s:23:\"container_padding_right\";s:3:\"0px\";s:24:\"container_padding_bottom\";s:3:\"0px\";s:22:\"container_padding_left\";s:3:\"0px\";s:32:\"container_border_radius_top_left\";s:3:\"0px\";s:33:\"container_border_radius_top_right\";s:3:\"0px\";s:36:\"container_border_radius_bottom_right\";s:3:\"0px\";s:35:\"container_border_radius_bottom_left\";s:3:\"0px\";s:15:\"menu_item_align\";s:6:\"center\";s:20:\"menu_item_link_color\";s:7:\"#ffffff\";s:24:\"menu_item_link_font_size\";s:4:\"14px\";s:19:\"menu_item_link_font\";s:7:\"inherit\";s:29:\"menu_item_link_text_transform\";s:4:\"none\";s:21:\"menu_item_link_weight\";s:6:\"normal\";s:30:\"menu_item_link_text_decoration\";s:4:\"none\";s:25:\"menu_item_link_text_align\";s:4:\"left\";s:26:\"menu_item_link_color_hover\";s:7:\"#ffffff\";s:27:\"menu_item_link_weight_hover\";s:6:\"normal\";s:36:\"menu_item_link_text_decoration_hover\";s:4:\"none\";s:25:\"menu_item_background_from\";s:13:\"rgba(0,0,0,0)\";s:23:\"menu_item_background_to\";s:13:\"rgba(0,0,0,0)\";s:31:\"menu_item_background_hover_from\";s:4:\"#333\";s:29:\"menu_item_background_hover_to\";s:4:\"#333\";s:17:\"menu_item_spacing\";s:3:\"0px\";s:26:\"menu_item_link_padding_top\";s:3:\"0px\";s:28:\"menu_item_link_padding_right\";s:4:\"10px\";s:29:\"menu_item_link_padding_bottom\";s:3:\"0px\";s:27:\"menu_item_link_padding_left\";s:4:\"10px\";s:22:\"menu_item_border_color\";s:4:\"#fff\";s:28:\"menu_item_border_color_hover\";s:4:\"#fff\";s:20:\"menu_item_border_top\";s:3:\"0px\";s:22:\"menu_item_border_right\";s:3:\"0px\";s:23:\"menu_item_border_bottom\";s:3:\"0px\";s:21:\"menu_item_border_left\";s:3:\"0px\";s:37:\"menu_item_link_border_radius_top_left\";s:3:\"0px\";s:38:\"menu_item_link_border_radius_top_right\";s:3:\"0px\";s:41:\"menu_item_link_border_radius_bottom_right\";s:3:\"0px\";s:40:\"menu_item_link_border_radius_bottom_left\";s:3:\"0px\";s:23:\"menu_item_divider_color\";s:24:\"rgba(255, 255, 255, 0.1)\";s:30:\"menu_item_divider_glow_opacity\";s:3:\"0.1\";s:27:\"menu_item_highlight_current\";s:2:\"on\";s:21:\"panel_background_from\";s:7:\"#f1f1f1\";s:19:\"panel_background_to\";s:7:\"#f1f1f1\";s:11:\"panel_width\";s:4:\"100%\";s:17:\"panel_inner_width\";s:4:\"100%\";s:17:\"panel_padding_top\";s:3:\"0px\";s:19:\"panel_padding_right\";s:3:\"0px\";s:20:\"panel_padding_bottom\";s:3:\"0px\";s:18:\"panel_padding_left\";s:3:\"0px\";s:18:\"panel_border_color\";s:4:\"#fff\";s:16:\"panel_border_top\";s:3:\"0px\";s:18:\"panel_border_right\";s:3:\"0px\";s:19:\"panel_border_bottom\";s:3:\"0px\";s:17:\"panel_border_left\";s:3:\"0px\";s:28:\"panel_border_radius_top_left\";s:3:\"0px\";s:29:\"panel_border_radius_top_right\";s:3:\"0px\";s:32:\"panel_border_radius_bottom_right\";s:3:\"0px\";s:31:\"panel_border_radius_bottom_left\";s:3:\"0px\";s:24:\"panel_widget_padding_top\";s:4:\"15px\";s:26:\"panel_widget_padding_right\";s:4:\"15px\";s:27:\"panel_widget_padding_bottom\";s:4:\"15px\";s:25:\"panel_widget_padding_left\";s:4:\"15px\";s:18:\"panel_header_color\";s:4:\"#555\";s:22:\"panel_header_font_size\";s:4:\"16px\";s:17:\"panel_header_font\";s:7:\"inherit\";s:27:\"panel_header_text_transform\";s:9:\"uppercase\";s:24:\"panel_header_font_weight\";s:4:\"bold\";s:28:\"panel_header_text_decoration\";s:4:\"none\";s:23:\"panel_header_text_align\";s:4:\"left\";s:24:\"panel_header_padding_top\";s:3:\"0px\";s:26:\"panel_header_padding_right\";s:3:\"0px\";s:27:\"panel_header_padding_bottom\";s:3:\"5px\";s:25:\"panel_header_padding_left\";s:3:\"0px\";s:23:\"panel_header_margin_top\";s:3:\"0px\";s:25:\"panel_header_margin_right\";s:3:\"0px\";s:26:\"panel_header_margin_bottom\";s:3:\"0px\";s:24:\"panel_header_margin_left\";s:3:\"0px\";s:25:\"panel_header_border_color\";s:13:\"rgba(0,0,0,0)\";s:31:\"panel_header_border_color_hover\";s:13:\"rgba(0,0,0,0)\";s:23:\"panel_header_border_top\";s:3:\"0px\";s:25:\"panel_header_border_right\";s:3:\"0px\";s:26:\"panel_header_border_bottom\";s:3:\"0px\";s:24:\"panel_header_border_left\";s:3:\"0px\";s:16:\"panel_font_color\";s:4:\"#666\";s:15:\"panel_font_size\";s:4:\"14px\";s:17:\"panel_font_family\";s:7:\"inherit\";s:29:\"panel_second_level_font_color\";s:4:\"#555\";s:28:\"panel_second_level_font_size\";s:4:\"16px\";s:23:\"panel_second_level_font\";s:7:\"inherit\";s:33:\"panel_second_level_text_transform\";s:9:\"uppercase\";s:30:\"panel_second_level_font_weight\";s:4:\"bold\";s:34:\"panel_second_level_text_decoration\";s:4:\"none\";s:29:\"panel_second_level_text_align\";s:4:\"left\";s:35:\"panel_second_level_font_color_hover\";s:4:\"#555\";s:36:\"panel_second_level_font_weight_hover\";s:4:\"bold\";s:40:\"panel_second_level_text_decoration_hover\";s:4:\"none\";s:40:\"panel_second_level_background_hover_from\";s:13:\"rgba(0,0,0,0)\";s:38:\"panel_second_level_background_hover_to\";s:13:\"rgba(0,0,0,0)\";s:30:\"panel_second_level_padding_top\";s:3:\"0px\";s:32:\"panel_second_level_padding_right\";s:3:\"0px\";s:33:\"panel_second_level_padding_bottom\";s:3:\"0px\";s:31:\"panel_second_level_padding_left\";s:3:\"0px\";s:29:\"panel_second_level_margin_top\";s:3:\"0px\";s:31:\"panel_second_level_margin_right\";s:3:\"0px\";s:32:\"panel_second_level_margin_bottom\";s:3:\"0px\";s:30:\"panel_second_level_margin_left\";s:3:\"0px\";s:31:\"panel_second_level_border_color\";s:13:\"rgba(0,0,0,0)\";s:37:\"panel_second_level_border_color_hover\";s:13:\"rgba(0,0,0,0)\";s:29:\"panel_second_level_border_top\";s:3:\"0px\";s:31:\"panel_second_level_border_right\";s:3:\"0px\";s:32:\"panel_second_level_border_bottom\";s:3:\"0px\";s:30:\"panel_second_level_border_left\";s:3:\"0px\";s:28:\"panel_third_level_font_color\";s:4:\"#666\";s:27:\"panel_third_level_font_size\";s:4:\"14px\";s:22:\"panel_third_level_font\";s:7:\"inherit\";s:32:\"panel_third_level_text_transform\";s:4:\"none\";s:29:\"panel_third_level_font_weight\";s:6:\"normal\";s:33:\"panel_third_level_text_decoration\";s:4:\"none\";s:28:\"panel_third_level_text_align\";s:4:\"left\";s:34:\"panel_third_level_font_color_hover\";s:4:\"#666\";s:35:\"panel_third_level_font_weight_hover\";s:6:\"normal\";s:39:\"panel_third_level_text_decoration_hover\";s:4:\"none\";s:39:\"panel_third_level_background_hover_from\";s:13:\"rgba(0,0,0,0)\";s:37:\"panel_third_level_background_hover_to\";s:13:\"rgba(0,0,0,0)\";s:29:\"panel_third_level_padding_top\";s:3:\"0px\";s:31:\"panel_third_level_padding_right\";s:3:\"0px\";s:32:\"panel_third_level_padding_bottom\";s:3:\"0px\";s:30:\"panel_third_level_padding_left\";s:3:\"0px\";s:28:\"panel_third_level_margin_top\";s:3:\"0px\";s:30:\"panel_third_level_margin_right\";s:3:\"0px\";s:31:\"panel_third_level_margin_bottom\";s:3:\"0px\";s:29:\"panel_third_level_margin_left\";s:3:\"0px\";s:30:\"panel_third_level_border_color\";s:13:\"rgba(0,0,0,0)\";s:36:\"panel_third_level_border_color_hover\";s:13:\"rgba(0,0,0,0)\";s:28:\"panel_third_level_border_top\";s:3:\"0px\";s:30:\"panel_third_level_border_right\";s:3:\"0px\";s:31:\"panel_third_level_border_bottom\";s:3:\"0px\";s:29:\"panel_third_level_border_left\";s:3:\"0px\";s:27:\"flyout_menu_background_from\";s:7:\"#f1f1f1\";s:25:\"flyout_menu_background_to\";s:7:\"#f1f1f1\";s:12:\"flyout_width\";s:5:\"250px\";s:18:\"flyout_padding_top\";s:3:\"0px\";s:20:\"flyout_padding_right\";s:3:\"0px\";s:21:\"flyout_padding_bottom\";s:3:\"0px\";s:19:\"flyout_padding_left\";s:3:\"0px\";s:19:\"flyout_border_color\";s:7:\"#ffffff\";s:17:\"flyout_border_top\";s:3:\"0px\";s:19:\"flyout_border_right\";s:3:\"0px\";s:20:\"flyout_border_bottom\";s:3:\"0px\";s:18:\"flyout_border_left\";s:3:\"0px\";s:29:\"flyout_border_radius_top_left\";s:3:\"0px\";s:30:\"flyout_border_radius_top_right\";s:3:\"0px\";s:33:\"flyout_border_radius_bottom_right\";s:3:\"0px\";s:32:\"flyout_border_radius_bottom_left\";s:3:\"0px\";s:22:\"flyout_background_from\";s:7:\"#f1f1f1\";s:20:\"flyout_background_to\";s:7:\"#f1f1f1\";s:28:\"flyout_background_hover_from\";s:7:\"#dddddd\";s:26:\"flyout_background_hover_to\";s:7:\"#dddddd\";s:18:\"flyout_link_height\";s:4:\"35px\";s:23:\"flyout_link_padding_top\";s:3:\"0px\";s:25:\"flyout_link_padding_right\";s:4:\"10px\";s:26:\"flyout_link_padding_bottom\";s:3:\"0px\";s:24:\"flyout_link_padding_left\";s:4:\"10px\";s:17:\"flyout_link_color\";s:4:\"#666\";s:16:\"flyout_link_size\";s:4:\"14px\";s:18:\"flyout_link_family\";s:7:\"inherit\";s:26:\"flyout_link_text_transform\";s:4:\"none\";s:18:\"flyout_link_weight\";s:6:\"normal\";s:27:\"flyout_link_text_decoration\";s:4:\"none\";s:23:\"flyout_link_color_hover\";s:4:\"#666\";s:24:\"flyout_link_weight_hover\";s:6:\"normal\";s:33:\"flyout_link_text_decoration_hover\";s:4:\"none\";s:30:\"flyout_menu_item_divider_color\";s:24:\"rgba(255, 255, 255, 0.1)\";s:21:\"responsive_breakpoint\";s:5:\"768px\";s:22:\"toggle_background_from\";s:4:\"#222\";s:20:\"toggle_background_to\";s:4:\"#222\";s:17:\"toggle_bar_height\";s:4:\"40px\";s:33:\"toggle_bar_border_radius_top_left\";s:3:\"2px\";s:34:\"toggle_bar_border_radius_top_right\";s:3:\"2px\";s:37:\"toggle_bar_border_radius_bottom_right\";s:3:\"2px\";s:36:\"toggle_bar_border_radius_bottom_left\";s:3:\"2px\";s:28:\"mobile_menu_off_canvas_width\";s:5:\"300px\";s:23:\"mobile_menu_item_height\";s:4:\"40px\";s:23:\"mobile_menu_padding_top\";s:3:\"0px\";s:25:\"mobile_menu_padding_right\";s:3:\"0px\";s:26:\"mobile_menu_padding_bottom\";s:3:\"0px\";s:24:\"mobile_menu_padding_left\";s:3:\"0px\";s:22:\"mobile_background_from\";s:4:\"#222\";s:20:\"mobile_background_to\";s:4:\"#222\";s:38:\"mobile_menu_item_background_hover_from\";s:4:\"#333\";s:36:\"mobile_menu_item_background_hover_to\";s:4:\"#333\";s:27:\"mobile_menu_item_link_color\";s:7:\"#ffffff\";s:31:\"mobile_menu_item_link_font_size\";s:4:\"14px\";s:32:\"mobile_menu_item_link_text_align\";s:4:\"left\";s:33:\"mobile_menu_item_link_color_hover\";s:7:\"#ffffff\";s:14:\"mobile_columns\";s:1:\"1\";s:32:\"mobile_menu_force_width_selector\";s:4:\"body\";s:10:\"custom_css\";s:67:\"/** Push menu onto new line **/ \r\n#{$wrap} { \r\n    clear: both; \r\n}\";s:6:\"shadow\";s:3:\"off\";s:11:\"transitions\";s:3:\"off\";s:6:\"resets\";s:3:\"off\";s:17:\"menu_item_divider\";s:3:\"off\";s:24:\"flyout_menu_item_divider\";s:3:\"off\";s:21:\"disable_mobile_toggle\";s:3:\"off\";s:19:\"mobile_menu_overlay\";s:3:\"off\";s:23:\"mobile_menu_force_width\";s:3:\"off\";}}', 'yes'),
(286, 'megamenu_themes_last_updated', 'default', 'yes'),
(287, 'megamenu_toggle_blocks', 'a:1:{s:7:\"default\";a:1:{i:0;a:6:{s:4:\"type\";s:20:\"menu_toggle_animated\";s:5:\"align\";s:5:\"right\";s:5:\"style\";s:6:\"slider\";s:10:\"icon_color\";s:18:\"rgb(221, 221, 221)\";s:10:\"icon_scale\";s:3:\"0.8\";s:10:\"aria_label\";s:11:\"Toggle Menu\";}}}', 'yes'),
(297, 'theme_mods_shuttle-eshop', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:11:\"header_menu\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:19;}', 'yes'),
(298, 'shuttle_redux_variables', 'a:8:{s:26:\"shuttle_header_styleswitch\";s:7:\"option1\";s:18:\"shuttle_blog_style\";s:7:\"option2\";s:25:\"shuttle_blog_style1layout\";s:7:\"option1\";s:25:\"shuttle_blog_style2layout\";s:7:\"option2\";s:26:\"shuttle_styles_colorswitch\";s:0:\"\";s:26:\"shuttle_styles_colorcustom\";s:0:\"\";s:25:\"shuttle_styles_skinswitch\";s:1:\"1\";s:19:\"shuttle_styles_skin\";s:5:\"eshop\";}', 'yes'),
(299, 'shuttle_child_settings_eshop', '1', 'yes'),
(344, '_site_transient_timeout_php_check_ecfa741d55b7b1a85bd61a2307877c8c', '1632851031', 'no'),
(345, '_site_transient_php_check_ecfa741d55b7b1a85bd61a2307877c8c', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(346, '_site_transient_timeout_browser_c7e7172c7781b034963ef5178f1479dd', '1632851162', 'no'),
(347, '_site_transient_browser_c7e7172c7781b034963ef5178f1479dd', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"93.0.4577.63\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(370, 'category_children', 'a:0:{}', 'yes'),
(374, '_transient_is_multi_author', '0', 'yes'),
(375, '_transient_all_the_cool_cats', '1', 'yes'),
(391, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:4:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:62:\"https://downloads.wordpress.org/release/uk/wordpress-5.8.1.zip\";s:6:\"locale\";s:2:\"uk\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:62:\"https://downloads.wordpress.org/release/uk/wordpress-5.8.1.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.8.1\";s:7:\"version\";s:5:\"5.8.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.8.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.8.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.8.1\";s:7:\"version\";s:5:\"5.8.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:62:\"https://downloads.wordpress.org/release/uk/wordpress-5.8.1.zip\";s:6:\"locale\";s:2:\"uk\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:62:\"https://downloads.wordpress.org/release/uk/wordpress-5.8.1.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.8.1\";s:7:\"version\";s:5:\"5.8.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:62:\"https://downloads.wordpress.org/release/uk/wordpress-5.7.3.zip\";s:6:\"locale\";s:2:\"uk\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:62:\"https://downloads.wordpress.org/release/uk/wordpress-5.7.3.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.7.3\";s:7:\"version\";s:5:\"5.7.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1632464339;s:15:\"version_checked\";s:5:\"5.7.2\";s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-24 15:34:57\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/uk.zip\";s:10:\"autoupdate\";b:1;}}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(392, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1632464340;s:7:\"checked\";a:6:{s:13:\"shuttle-eshop\";s:5:\"1.0.7\";s:7:\"shuttle\";s:5:\"1.3.1\";s:5:\"theme\";s:5:\"2.0.4\";s:14:\"twentynineteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.7\";s:15:\"twentytwentyone\";s:3:\"1.3\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"2.1\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.2.1.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.8\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.8.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.4.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:9:\"no_update\";a:2:{s:13:\"shuttle-eshop\";a:6:{s:5:\"theme\";s:13:\"shuttle-eshop\";s:11:\"new_version\";s:5:\"1.0.7\";s:3:\"url\";s:43:\"https://wordpress.org/themes/shuttle-eshop/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/shuttle-eshop.1.0.7.zip\";s:8:\"requires\";b:0;s:12:\"requires_php\";b:0;}s:7:\"shuttle\";a:6:{s:5:\"theme\";s:7:\"shuttle\";s:11:\"new_version\";s:5:\"1.3.1\";s:3:\"url\";s:37:\"https://wordpress.org/themes/shuttle/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/theme/shuttle.1.3.1.zip\";s:8:\"requires\";s:3:\"5.0\";s:12:\"requires_php\";s:3:\"5.6\";}}s:12:\"translations\";a:0:{}}', 'no'),
(393, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1632464339;s:7:\"checked\";a:4:{s:33:\"aapside-master/appside-master.php\";s:5:\"2.0.4\";s:19:\"akismet/akismet.php\";s:5:\"4.1.9\";s:9:\"hello.php\";s:5:\"1.7.2\";s:21:\"megamenu/megamenu.php\";s:5:\"2.9.4\";}s:8:\"response\";a:1:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:6:\"4.1.12\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/akismet.4.1.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.6\";s:6:\"tested\";s:5:\"5.8.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"hello-dolly\";s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"1.7.2\";s:7:\"updated\";s:19:\"2019-06-07 20:53:10\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/translation/plugin/hello-dolly/1.7.2/uk.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:2:{s:9:\"hello.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.6\";}s:21:\"megamenu/megamenu.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:22:\"w.org/plugins/megamenu\";s:4:\"slug\";s:8:\"megamenu\";s:6:\"plugin\";s:21:\"megamenu/megamenu.php\";s:11:\"new_version\";s:5:\"2.9.4\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/megamenu/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/megamenu.2.9.4.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/megamenu/assets/icon-128x128.png?rev=1489843\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/megamenu/assets/banner-1544x500.png?rev=1933092\";s:2:\"1x\";s:63:\"https://ps.w.org/megamenu/assets/banner-772x250.png?rev=1933095\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";}}}', 'no'),
(395, '_site_transient_timeout_theme_roots', '1632466140', 'no'),
(396, '_site_transient_theme_roots', 'a:6:{s:13:\"shuttle-eshop\";s:7:\"/themes\";s:7:\"shuttle\";s:7:\"/themes\";s:5:\"theme\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}', 'no');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(13, 11, '_edit_lock', '1628434633:1'),
(14, 13, '_edit_lock', '1628434646:1'),
(42, 21, '_wp_attached_file', '2021/08/fism_logo.png'),
(43, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:122;s:6:\"height\";i:122;s:4:\"file\";s:21:\"2021/08/fism_logo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(44, 21, '_wp_attachment_image_alt', 'logo'),
(49, 25, '_edit_lock', '1632246871:1'),
(50, 27, '_edit_lock', '1632246835:1'),
(51, 29, '_edit_lock', '1632246862:1'),
(52, 31, '_edit_lock', '1632246922:1'),
(53, 33, '_edit_lock', '1632246955:1'),
(54, 35, '_edit_lock', '1632246977:1'),
(55, 37, '_edit_lock', '1632246991:1'),
(56, 39, '_edit_lock', '1632247009:1'),
(57, 41, '_edit_lock', '1632247026:1'),
(58, 43, '_edit_lock', '1632247038:1'),
(59, 45, '_edit_lock', '1632247049:1'),
(60, 47, '_edit_lock', '1632247063:1'),
(61, 49, '_edit_lock', '1632247075:1'),
(62, 51, '_edit_lock', '1632247086:1'),
(63, 53, '_edit_lock', '1632247096:1'),
(64, 55, '_edit_lock', '1632247109:1'),
(65, 57, '_edit_lock', '1632247120:1'),
(66, 59, '_edit_lock', '1632247154:1'),
(67, 61, '_edit_lock', '1632247171:1'),
(68, 63, '_edit_lock', '1632247181:1'),
(69, 65, '_edit_lock', '1632247190:1'),
(70, 67, '_edit_lock', '1632247201:1'),
(71, 69, '_edit_lock', '1632247216:1'),
(72, 71, '_edit_lock', '1632247226:1'),
(73, 73, '_edit_lock', '1632247236:1'),
(74, 75, '_edit_lock', '1632247245:1'),
(75, 77, '_edit_lock', '1632247255:1'),
(76, 79, '_edit_lock', '1632247271:1'),
(77, 81, '_edit_lock', '1632247286:1'),
(78, 83, '_edit_lock', '1632247297:1'),
(79, 85, '_edit_lock', '1632247309:1'),
(80, 87, '_edit_lock', '1632247328:1'),
(81, 89, '_menu_item_type', 'taxonomy'),
(82, 89, '_menu_item_menu_item_parent', '0'),
(83, 89, '_menu_item_object_id', '4'),
(84, 89, '_menu_item_object', 'category'),
(85, 89, '_menu_item_target', ''),
(86, 89, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(87, 89, '_menu_item_xfn', ''),
(88, 89, '_menu_item_url', ''),
(90, 90, '_menu_item_type', 'taxonomy'),
(91, 90, '_menu_item_menu_item_parent', '0'),
(92, 90, '_menu_item_object_id', '3'),
(93, 90, '_menu_item_object', 'category'),
(94, 90, '_menu_item_target', ''),
(95, 90, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(96, 90, '_menu_item_xfn', ''),
(97, 90, '_menu_item_url', ''),
(99, 91, '_menu_item_type', 'taxonomy'),
(100, 91, '_menu_item_menu_item_parent', '0'),
(101, 91, '_menu_item_object_id', '9'),
(102, 91, '_menu_item_object', 'category'),
(103, 91, '_menu_item_target', ''),
(104, 91, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(105, 91, '_menu_item_xfn', ''),
(106, 91, '_menu_item_url', ''),
(108, 92, '_menu_item_type', 'taxonomy'),
(109, 92, '_menu_item_menu_item_parent', '0'),
(110, 92, '_menu_item_object_id', '5'),
(111, 92, '_menu_item_object', 'category'),
(112, 92, '_menu_item_target', ''),
(113, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(114, 92, '_menu_item_xfn', ''),
(115, 92, '_menu_item_url', ''),
(117, 93, '_menu_item_type', 'taxonomy'),
(118, 93, '_menu_item_menu_item_parent', '0'),
(119, 93, '_menu_item_object_id', '6'),
(120, 93, '_menu_item_object', 'category'),
(121, 93, '_menu_item_target', ''),
(122, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(123, 93, '_menu_item_xfn', ''),
(124, 93, '_menu_item_url', ''),
(126, 94, '_menu_item_type', 'taxonomy'),
(127, 94, '_menu_item_menu_item_parent', '0'),
(128, 94, '_menu_item_object_id', '10'),
(129, 94, '_menu_item_object', 'category'),
(130, 94, '_menu_item_target', ''),
(131, 94, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(132, 94, '_menu_item_xfn', ''),
(133, 94, '_menu_item_url', ''),
(135, 95, '_menu_item_type', 'taxonomy'),
(136, 95, '_menu_item_menu_item_parent', '0'),
(137, 95, '_menu_item_object_id', '7'),
(138, 95, '_menu_item_object', 'category'),
(139, 95, '_menu_item_target', ''),
(140, 95, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(141, 95, '_menu_item_xfn', ''),
(142, 95, '_menu_item_url', ''),
(144, 96, '_menu_item_type', 'taxonomy'),
(145, 96, '_menu_item_menu_item_parent', '0'),
(146, 96, '_menu_item_object_id', '8'),
(147, 96, '_menu_item_object', 'category'),
(148, 96, '_menu_item_target', ''),
(149, 96, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(150, 96, '_menu_item_xfn', ''),
(151, 96, '_menu_item_url', ''),
(153, 97, '_menu_item_type', 'post_type'),
(154, 97, '_menu_item_menu_item_parent', '90'),
(155, 97, '_menu_item_object_id', '35'),
(156, 97, '_menu_item_object', 'page'),
(157, 97, '_menu_item_target', ''),
(158, 97, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(159, 97, '_menu_item_xfn', ''),
(160, 97, '_menu_item_url', ''),
(162, 98, '_menu_item_type', 'post_type'),
(163, 98, '_menu_item_menu_item_parent', '90'),
(164, 98, '_menu_item_object_id', '25'),
(165, 98, '_menu_item_object', 'page'),
(166, 98, '_menu_item_target', ''),
(167, 98, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(168, 98, '_menu_item_xfn', ''),
(169, 98, '_menu_item_url', ''),
(171, 99, '_menu_item_type', 'post_type'),
(172, 99, '_menu_item_menu_item_parent', '90'),
(173, 99, '_menu_item_object_id', '27'),
(174, 99, '_menu_item_object', 'page'),
(175, 99, '_menu_item_target', ''),
(176, 99, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(177, 99, '_menu_item_xfn', ''),
(178, 99, '_menu_item_url', ''),
(180, 100, '_menu_item_type', 'post_type'),
(181, 100, '_menu_item_menu_item_parent', '90'),
(182, 100, '_menu_item_object_id', '37'),
(183, 100, '_menu_item_object', 'page'),
(184, 100, '_menu_item_target', ''),
(185, 100, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(186, 100, '_menu_item_xfn', ''),
(187, 100, '_menu_item_url', ''),
(189, 101, '_menu_item_type', 'post_type'),
(190, 101, '_menu_item_menu_item_parent', '90'),
(191, 101, '_menu_item_object_id', '79'),
(192, 101, '_menu_item_object', 'page'),
(193, 101, '_menu_item_target', ''),
(194, 101, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(195, 101, '_menu_item_xfn', ''),
(196, 101, '_menu_item_url', ''),
(198, 102, '_menu_item_type', 'post_type'),
(199, 102, '_menu_item_menu_item_parent', '90'),
(200, 102, '_menu_item_object_id', '29'),
(201, 102, '_menu_item_object', 'page'),
(202, 102, '_menu_item_target', ''),
(203, 102, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(204, 102, '_menu_item_xfn', ''),
(205, 102, '_menu_item_url', ''),
(207, 103, '_menu_item_type', 'post_type'),
(208, 103, '_menu_item_menu_item_parent', '90'),
(209, 103, '_menu_item_object_id', '39'),
(210, 103, '_menu_item_object', 'page'),
(211, 103, '_menu_item_target', ''),
(212, 103, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(213, 103, '_menu_item_xfn', ''),
(214, 103, '_menu_item_url', ''),
(216, 104, '_menu_item_type', 'post_type'),
(217, 104, '_menu_item_menu_item_parent', '90'),
(218, 104, '_menu_item_object_id', '31'),
(219, 104, '_menu_item_object', 'page'),
(220, 104, '_menu_item_target', ''),
(221, 104, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(222, 104, '_menu_item_xfn', ''),
(223, 104, '_menu_item_url', ''),
(225, 105, '_menu_item_type', 'post_type'),
(226, 105, '_menu_item_menu_item_parent', '89'),
(227, 105, '_menu_item_object_id', '45'),
(228, 105, '_menu_item_object', 'page'),
(229, 105, '_menu_item_target', ''),
(230, 105, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(231, 105, '_menu_item_xfn', ''),
(232, 105, '_menu_item_url', ''),
(234, 106, '_menu_item_type', 'post_type'),
(235, 106, '_menu_item_menu_item_parent', '89'),
(236, 106, '_menu_item_object_id', '43'),
(237, 106, '_menu_item_object', 'page'),
(238, 106, '_menu_item_target', ''),
(239, 106, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(240, 106, '_menu_item_xfn', ''),
(241, 106, '_menu_item_url', ''),
(243, 107, '_menu_item_type', 'post_type'),
(244, 107, '_menu_item_menu_item_parent', '89'),
(245, 107, '_menu_item_object_id', '41'),
(246, 107, '_menu_item_object', 'page'),
(247, 107, '_menu_item_target', ''),
(248, 107, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(249, 107, '_menu_item_xfn', ''),
(250, 107, '_menu_item_url', ''),
(252, 108, '_menu_item_type', 'post_type'),
(253, 108, '_menu_item_menu_item_parent', '89'),
(254, 108, '_menu_item_object_id', '47'),
(255, 108, '_menu_item_object', 'page'),
(256, 108, '_menu_item_target', ''),
(257, 108, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(258, 108, '_menu_item_xfn', ''),
(259, 108, '_menu_item_url', ''),
(261, 109, '_menu_item_type', 'post_type'),
(262, 109, '_menu_item_menu_item_parent', '92'),
(263, 109, '_menu_item_object_id', '59'),
(264, 109, '_menu_item_object', 'page'),
(265, 109, '_menu_item_target', ''),
(266, 109, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(267, 109, '_menu_item_xfn', ''),
(268, 109, '_menu_item_url', ''),
(270, 110, '_menu_item_type', 'post_type'),
(271, 110, '_menu_item_menu_item_parent', '92'),
(272, 110, '_menu_item_object_id', '57'),
(273, 110, '_menu_item_object', 'page'),
(274, 110, '_menu_item_target', ''),
(275, 110, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(276, 110, '_menu_item_xfn', ''),
(277, 110, '_menu_item_url', ''),
(279, 111, '_menu_item_type', 'post_type'),
(280, 111, '_menu_item_menu_item_parent', '92'),
(281, 111, '_menu_item_object_id', '55'),
(282, 111, '_menu_item_object', 'page'),
(283, 111, '_menu_item_target', ''),
(284, 111, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(285, 111, '_menu_item_xfn', ''),
(286, 111, '_menu_item_url', ''),
(288, 112, '_menu_item_type', 'post_type'),
(289, 112, '_menu_item_menu_item_parent', '92'),
(290, 112, '_menu_item_object_id', '51'),
(291, 112, '_menu_item_object', 'page'),
(292, 112, '_menu_item_target', ''),
(293, 112, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(294, 112, '_menu_item_xfn', ''),
(295, 112, '_menu_item_url', ''),
(297, 113, '_menu_item_type', 'post_type'),
(298, 113, '_menu_item_menu_item_parent', '92'),
(299, 113, '_menu_item_object_id', '53'),
(300, 113, '_menu_item_object', 'page'),
(301, 113, '_menu_item_target', ''),
(302, 113, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(303, 113, '_menu_item_xfn', ''),
(304, 113, '_menu_item_url', ''),
(306, 114, '_menu_item_type', 'post_type'),
(307, 114, '_menu_item_menu_item_parent', '92'),
(308, 114, '_menu_item_object_id', '49'),
(309, 114, '_menu_item_object', 'page'),
(310, 114, '_menu_item_target', ''),
(311, 114, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(312, 114, '_menu_item_xfn', ''),
(313, 114, '_menu_item_url', ''),
(315, 115, '_menu_item_type', 'post_type'),
(316, 115, '_menu_item_menu_item_parent', '93'),
(317, 115, '_menu_item_object_id', '65'),
(318, 115, '_menu_item_object', 'page'),
(319, 115, '_menu_item_target', ''),
(320, 115, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(321, 115, '_menu_item_xfn', ''),
(322, 115, '_menu_item_url', ''),
(324, 116, '_menu_item_type', 'post_type'),
(325, 116, '_menu_item_menu_item_parent', '93'),
(326, 116, '_menu_item_object_id', '69'),
(327, 116, '_menu_item_object', 'page'),
(328, 116, '_menu_item_target', ''),
(329, 116, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(330, 116, '_menu_item_xfn', ''),
(331, 116, '_menu_item_url', ''),
(333, 117, '_menu_item_type', 'post_type'),
(334, 117, '_menu_item_menu_item_parent', '93'),
(335, 117, '_menu_item_object_id', '67'),
(336, 117, '_menu_item_object', 'page'),
(337, 117, '_menu_item_target', ''),
(338, 117, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(339, 117, '_menu_item_xfn', ''),
(340, 117, '_menu_item_url', ''),
(342, 118, '_menu_item_type', 'post_type'),
(343, 118, '_menu_item_menu_item_parent', '93'),
(344, 118, '_menu_item_object_id', '61'),
(345, 118, '_menu_item_object', 'page'),
(346, 118, '_menu_item_target', ''),
(347, 118, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(348, 118, '_menu_item_xfn', ''),
(349, 118, '_menu_item_url', ''),
(351, 119, '_menu_item_type', 'post_type'),
(352, 119, '_menu_item_menu_item_parent', '93'),
(353, 119, '_menu_item_object_id', '63'),
(354, 119, '_menu_item_object', 'page'),
(355, 119, '_menu_item_target', ''),
(356, 119, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(357, 119, '_menu_item_xfn', ''),
(358, 119, '_menu_item_url', ''),
(360, 120, '_menu_item_type', 'post_type'),
(361, 120, '_menu_item_menu_item_parent', '96'),
(362, 120, '_menu_item_object_id', '77'),
(363, 120, '_menu_item_object', 'page'),
(364, 120, '_menu_item_target', ''),
(365, 120, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(366, 120, '_menu_item_xfn', ''),
(367, 120, '_menu_item_url', ''),
(369, 121, '_menu_item_type', 'post_type'),
(370, 121, '_menu_item_menu_item_parent', '96'),
(371, 121, '_menu_item_object_id', '71'),
(372, 121, '_menu_item_object', 'page'),
(373, 121, '_menu_item_target', ''),
(374, 121, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(375, 121, '_menu_item_xfn', ''),
(376, 121, '_menu_item_url', ''),
(378, 122, '_menu_item_type', 'post_type'),
(379, 122, '_menu_item_menu_item_parent', '96'),
(380, 122, '_menu_item_object_id', '73'),
(381, 122, '_menu_item_object', 'page'),
(382, 122, '_menu_item_target', ''),
(383, 122, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(384, 122, '_menu_item_xfn', ''),
(385, 122, '_menu_item_url', ''),
(387, 123, '_menu_item_type', 'post_type'),
(388, 123, '_menu_item_menu_item_parent', '96'),
(389, 123, '_menu_item_object_id', '75'),
(390, 123, '_menu_item_object', 'page'),
(391, 123, '_menu_item_target', ''),
(392, 123, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(393, 123, '_menu_item_xfn', ''),
(394, 123, '_menu_item_url', ''),
(396, 124, '_menu_item_type', 'post_type'),
(397, 124, '_menu_item_menu_item_parent', '91'),
(398, 124, '_menu_item_object_id', '81'),
(399, 124, '_menu_item_object', 'page'),
(400, 124, '_menu_item_target', ''),
(401, 124, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(402, 124, '_menu_item_xfn', ''),
(403, 124, '_menu_item_url', ''),
(405, 125, '_menu_item_type', 'post_type'),
(406, 125, '_menu_item_menu_item_parent', '91'),
(407, 125, '_menu_item_object_id', '33'),
(408, 125, '_menu_item_object', 'page'),
(409, 125, '_menu_item_target', ''),
(410, 125, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(411, 125, '_menu_item_xfn', ''),
(412, 125, '_menu_item_url', ''),
(414, 126, '_menu_item_type', 'post_type'),
(415, 126, '_menu_item_menu_item_parent', '91'),
(416, 126, '_menu_item_object_id', '83'),
(417, 126, '_menu_item_object', 'page'),
(418, 126, '_menu_item_target', ''),
(419, 126, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(420, 126, '_menu_item_xfn', ''),
(421, 126, '_menu_item_url', ''),
(423, 127, '_menu_item_type', 'post_type'),
(424, 127, '_menu_item_menu_item_parent', '94'),
(425, 127, '_menu_item_object_id', '85'),
(426, 127, '_menu_item_object', 'page'),
(427, 127, '_menu_item_target', ''),
(428, 127, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(429, 127, '_menu_item_xfn', ''),
(430, 127, '_menu_item_url', ''),
(432, 128, '_menu_item_type', 'post_type'),
(433, 128, '_menu_item_menu_item_parent', '94'),
(434, 128, '_menu_item_object_id', '87'),
(435, 128, '_menu_item_object', 'page'),
(436, 128, '_menu_item_target', ''),
(437, 128, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(438, 128, '_menu_item_xfn', ''),
(439, 128, '_menu_item_url', '');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-07-23 20:08:05', '2021-07-23 17:08:05', '<!-- wp:paragraph -->\n<p>Ласкаво просимо до WordPress. Це ваш перший запис. Редагуйте або видаліть, а потім починайте писати!</p>\n<!-- /wp:paragraph -->', 'Привіт, світ!', '', 'publish', 'open', 'open', '', '%d0%bf%d1%80%d0%b8%d0%b2%d1%96%d1%82-%d1%81%d0%b2%d1%96%d1%82', '', '', '2021-07-23 20:08:05', '2021-07-23 17:08:05', '', 0, 'http://localhost/departmentsite/wordpress/?p=1', 0, 'post', '', 1),
(2, 1, '2021-07-23 20:08:05', '2021-07-23 17:08:05', '<!-- wp:paragraph -->\n<p>Це приклад сторінки. Від записів у блозі вона відрізняється тим, що залишається на одному місці і відображається в меню сайту (у більшості тем). На сторінці &laquo;Деталі&raquo; власники сайтів зазвичай розповідають про себе потенційним відвідувачам. Наприклад, так:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Привіт! Вдень я кур\'єр, а ввечері &#8212; перспективний актор. Це мій блог. Я живу у Львові, люблю свого пса Джека і Пінаколаду. (І ще потрапляти під дощ.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>... чи щось подібне до цього:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Компанія &laquo;Штучки XYZ&raquo; була заснована в 1971 році і з тих пір виготовляє якісні штучки. Компанія знаходиться в Готем-сіті, має штат з більш ніж 2000 співробітників і приносить багато користі жителям Готема.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Перейдіть <a href=\"http://localhost/departmentsite/wordpress/wp-admin/\">в майстерню</a>, щоб видалити цю сторінку і створити нові. Успіхів!</p>\n<!-- /wp:paragraph -->', 'Зразок сторінки', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2021-07-23 20:08:05', '2021-07-23 17:08:05', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-07-23 20:08:05', '2021-07-23 17:08:05', '<!-- wp:heading --><h2>Хто ми</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Наша адреса сайту: http://localhost/departmentsite/wordpress.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Коментарі</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Коли відвідувачі залишають коментарі на сайті, ми збираємо дані, що відображаються у формі коментарів, а також IP-адреси відвідувачів та рядку агента-браузера користувача, щоб допомогти виявити спам.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Анонімний рядок, створений за вашою адресою електронної пошти (також називається хеш), може бути наданий службі Gravatar, щоб дізнатись, чи ви його використовуєте. Політика конфіденційності служби Gravatar доступна тут: https://automattic.com/privacy/. Після схвалення вашого коментаря, ваше зображення профілю буде видно для громадськості в контексті вашого коментарю.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Медіафайли</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Якщо ви завантажуєте зображення на сайт, вам слід уникати завантаження зображень із вбудованими даними про місцезнаходження (EXIF GPS). Відвідувачі сайту можуть завантажувати та витягувати будь-які дані про місцезнаходження із зображень на сайті.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Якщо ви залишаєте коментар на нашому сайті, ви можете ввімкнути збереження свого імені, електронної адреси та сайту в файлах cookie. Це для вашої зручності, так що вам не потрібно буде повторно заповнювати ваші дані, коли ви залишатимете наступний коментар. Ці файли cookie зберігатимуться 1 рік.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Якщо у вас є обліковий запис на сайті і ви ввійдете в нього, ми встановимо тимчасовий cookie для визначення підтримки cookies вашим браузером, cookie не містить ніякої особистої інформації і віддаляється при закритті вашого браузера.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Коли ви ввійдете в систему, ми також встановимо декілька файлів cookie, щоб зберегти інформацію про ваш логін та налаштування екрана. Cookie-файли для входу зберігаються 2 дні, а файли cookie-файлів налаштувань екрану - 1 рік. Якщо ви виберете &quot;Запам\'ятати мене&quot;, ваш логін буде зберігатися протягом 2 тижнів. Якщо ви вийдете зі свого облікового запису, файли cookie логіну будуть видалені.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Якщо ви редагуєте або публікуєте статтю, у вашому веб-переглядачі буде збережений додатковий файл cookie. Цей файл cookie не містить особистих даних і просто вказує ідентифікатор статті, яку ви щойно редагували. Його термін дії закінчується через 1 день.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Вбудований вміст з інших веб-сайтів</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Статті на цьому сайті можуть містити вбудований вміст (наприклад: відео, зображення, статті тощо). Вбудований вміст з інших сайтів веде себе так само, як би користувач відвідав інший сайт.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Ці сайти можуть збирати дані про вас, використовувати файли cookie, вбудовані додатки відстеження третіх сторін та стежити за вашою взаємодією з цим вбудованим вмістом. Зокрема відстежувати взаємодію з вбудованим вмістом, якщо у вас є обліковий запис і ви увійшли на цей сайт.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>З ким ми ділимося вашими даними</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Якщо ви запросите скидання паролю, ваш IP буде вказано в email-повідомленні про скидання.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Як довго ми зберігаємо ваші дані</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Якщо ви залишаєте коментар, він та його метадані зберігаються протягом невизначеного терміну. Таким чином, ми можемо автоматично визначати та затверджувати кожен наступний коментар замість того, щоб тримати їх у черзі на модерації.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Для користувачів, які реєструються на нашому сайті (якщо такі є), ми зберігаємо надану ними персональну інформацію у їхньому профілі користувача. Всі користувачі можуть переглядати, редагувати або видаляти свої особисті дані в будь-який час (за винятком того, що вони не можуть змінити своє ім\'я користувача). Адміністратори сайту також можуть переглядати та редагувати цю інформацію.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Які права ви маєте відносно своїх даних</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Якщо у вас є обліковий запис на цьому сайті або ви залишили коментарі, ви можете подати запит на отримання експортованого файлу особистих даних які ми зберігаємо про вас, включаючи всі дані, які ви надали нам. Ви також можете вимагати, щоб ми стерли будь-які особисті дані, які ми маємо щодо вас. Це не включає будь-які дані, які ми зобов\'язані зберігати в адміністративних, правових та цілях безпеки.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Куди ми відправляємо ваші данні</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Пропонований текст: </strong>Коментарі відвідувачів можуть бути перевірені за допомогою служби автоматичного виявлення спаму.</p><!-- /wp:paragraph -->', 'Політика конфіденційності', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-07-23 20:08:05', '2021-07-23 17:08:05', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=3', 0, 'page', '', 0),
(11, 1, '2021-08-08 17:59:21', '2021-08-08 14:59:21', '', 'Загальна інформація', '', 'publish', 'closed', 'closed', '', '%d0%b7%d0%b0%d0%b3%d0%b0%d0%bb%d1%8c%d0%bd%d0%b0-%d1%96%d0%bd%d1%84%d0%be%d1%80%d0%bc%d0%b0%d1%86%d1%96%d1%8f', '', '', '2021-08-08 17:59:21', '2021-08-08 14:59:21', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=11', 0, 'page', '', 0),
(12, 1, '2021-08-08 17:59:21', '2021-08-08 14:59:21', '', 'Загальна інформація', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2021-08-08 17:59:21', '2021-08-08 14:59:21', '', 11, 'http://localhost/departmentsite/wordpress/?p=12', 0, 'revision', '', 0),
(13, 1, '2021-08-08 17:59:49', '2021-08-08 14:59:49', '', 'Вступ', '', 'publish', 'closed', 'closed', '', '%d0%b2%d1%81%d1%82%d1%83%d0%bf', '', '', '2021-08-08 17:59:49', '2021-08-08 14:59:49', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=13', 0, 'page', '', 0),
(14, 1, '2021-08-08 17:59:49', '2021-08-08 14:59:49', '', 'Вступ', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2021-08-08 17:59:49', '2021-08-08 14:59:49', '', 13, 'http://localhost/departmentsite/wordpress/?p=14', 0, 'revision', '', 0),
(21, 1, '2021-08-08 18:46:24', '2021-08-08 15:46:24', '', 'fism_logo', '', 'inherit', 'open', 'closed', '', 'fism_logo', '', '', '2021-08-08 18:46:30', '2021-08-08 15:46:30', '', 0, 'http://localhost/departmentsite/wordpress/wp-content/uploads/2021/08/fism_logo.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2021-09-21 20:46:03', '0000-00-00 00:00:00', '', 'Авточернетка', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-09-21 20:46:03', '0000-00-00 00:00:00', '', 0, 'http://localhost/departmentsite/wordpress/?p=24', 0, 'post', '', 0),
(25, 1, '2021-09-21 20:55:17', '2021-09-21 17:55:17', '', 'Історія', '', 'publish', 'closed', 'closed', '', '%d1%96%d1%81%d1%82%d0%be%d1%80%d1%96%d1%8f', '', '', '2021-09-21 20:55:17', '2021-09-21 17:55:17', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=25', 0, 'page', '', 0),
(26, 1, '2021-09-21 20:55:17', '2021-09-21 17:55:17', '', 'Історія', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-09-21 20:55:17', '2021-09-21 17:55:17', '', 25, 'http://localhost/departmentsite/wordpress/?p=26', 0, 'revision', '', 0),
(27, 1, '2021-09-21 20:56:17', '2021-09-21 17:56:17', '', 'Керівництво', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%b5%d1%80%d1%96%d0%b2%d0%bd%d0%b8%d1%86%d1%82%d0%b2%d0%be', '', '', '2021-09-21 20:56:17', '2021-09-21 17:56:17', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=27', 0, 'page', '', 0),
(28, 1, '2021-09-21 20:56:17', '2021-09-21 17:56:17', '', 'Керівництво', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-09-21 20:56:17', '2021-09-21 17:56:17', '', 27, 'http://localhost/departmentsite/wordpress/?p=28', 0, 'revision', '', 0),
(29, 1, '2021-09-21 20:56:34', '2021-09-21 17:56:34', '', 'Положення про підрозділ', '', 'publish', 'closed', 'closed', '', '%d0%bf%d0%be%d0%bb%d0%be%d0%b6%d0%b5%d0%bd%d0%bd%d1%8f-%d0%bf%d1%80%d0%be-%d0%bf%d1%96%d0%b4%d1%80%d0%be%d0%b7%d0%b4%d1%96%d0%bb', '', '', '2021-09-21 20:56:34', '2021-09-21 17:56:34', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=29', 0, 'page', '', 0),
(30, 1, '2021-09-21 20:56:34', '2021-09-21 17:56:34', '', 'Положення про підрозділ', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2021-09-21 20:56:34', '2021-09-21 17:56:34', '', 29, 'http://localhost/departmentsite/wordpress/?p=30', 0, 'revision', '', 0),
(31, 1, '2021-09-21 20:57:06', '2021-09-21 17:57:06', '', 'Розвиток та пріоритети', '', 'publish', 'closed', 'closed', '', '%d1%80%d0%be%d0%b7%d0%b2%d0%b8%d1%82%d0%be%d0%ba-%d1%82%d0%b0-%d0%bf%d1%80%d1%96%d0%be%d1%80%d0%b8%d1%82%d0%b5%d1%82%d0%b8', '', '', '2021-09-21 20:57:06', '2021-09-21 17:57:06', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=31', 0, 'page', '', 0),
(32, 1, '2021-09-21 20:57:06', '2021-09-21 17:57:06', '', 'Розвиток та пріоритети', '', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2021-09-21 20:57:06', '2021-09-21 17:57:06', '', 31, 'http://localhost/departmentsite/wordpress/?p=32', 0, 'revision', '', 0),
(33, 1, '2021-09-21 20:58:16', '2021-09-21 17:58:16', '', 'Партнери', '', 'publish', 'closed', 'closed', '', '%d0%bf%d0%b0%d1%80%d1%82%d0%bd%d0%b5%d1%80%d0%b8', '', '', '2021-09-21 20:58:16', '2021-09-21 17:58:16', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=33', 0, 'page', '', 0),
(34, 1, '2021-09-21 20:58:16', '2021-09-21 17:58:16', '', 'Партнери', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2021-09-21 20:58:16', '2021-09-21 17:58:16', '', 33, 'http://localhost/departmentsite/wordpress/?p=34', 0, 'revision', '', 0),
(35, 1, '2021-09-21 20:58:27', '2021-09-21 17:58:27', '', 'Випускники', '', 'publish', 'closed', 'closed', '', '%d0%b2%d0%b8%d0%bf%d1%83%d1%81%d0%ba%d0%bd%d0%b8%d0%ba%d0%b8', '', '', '2021-09-21 20:58:27', '2021-09-21 17:58:27', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=35', 0, 'page', '', 0),
(36, 1, '2021-09-21 20:58:27', '2021-09-21 17:58:27', '', 'Випускники', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2021-09-21 20:58:27', '2021-09-21 17:58:27', '', 35, 'http://localhost/departmentsite/wordpress/?p=36', 0, 'revision', '', 0),
(37, 1, '2021-09-21 20:58:50', '2021-09-21 17:58:50', '', 'Контактна інформація', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%bd%d0%b0-%d1%96%d0%bd%d1%84%d0%be%d1%80%d0%bc%d0%b0%d1%86%d1%96%d1%8f', '', '', '2021-09-21 20:58:50', '2021-09-21 17:58:50', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=37', 0, 'page', '', 0),
(38, 1, '2021-09-21 20:58:50', '2021-09-21 17:58:50', '', 'Контактна інформація', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2021-09-21 20:58:50', '2021-09-21 17:58:50', '', 37, 'http://localhost/departmentsite/wordpress/?p=38', 0, 'revision', '', 0),
(39, 1, '2021-09-21 20:59:03', '2021-09-21 17:59:03', '', 'Профспілка', '', 'publish', 'closed', 'closed', '', '%d0%bf%d1%80%d0%be%d1%84%d1%81%d0%bf%d1%96%d0%bb%d0%ba%d0%b0', '', '', '2021-09-21 20:59:03', '2021-09-21 17:59:03', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=39', 0, 'page', '', 0),
(40, 1, '2021-09-21 20:59:03', '2021-09-21 17:59:03', '', 'Профспілка', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2021-09-21 20:59:03', '2021-09-21 17:59:03', '', 39, 'http://localhost/departmentsite/wordpress/?p=40', 0, 'revision', '', 0),
(41, 1, '2021-09-21 20:59:20', '2021-09-21 17:59:20', '', 'Вступ на бакалаврат', '', 'publish', 'closed', 'closed', '', '%d0%b2%d1%81%d1%82%d1%83%d0%bf-%d0%bd%d0%b0-%d0%b1%d0%b0%d0%ba%d0%b0%d0%bb%d0%b0%d0%b2%d1%80%d0%b0%d1%82', '', '', '2021-09-21 20:59:20', '2021-09-21 17:59:20', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=41', 0, 'page', '', 0),
(42, 1, '2021-09-21 20:59:20', '2021-09-21 17:59:20', '', 'Вступ на бакалаврат', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2021-09-21 20:59:20', '2021-09-21 17:59:20', '', 41, 'http://localhost/departmentsite/wordpress/?p=42', 0, 'revision', '', 0),
(43, 1, '2021-09-21 20:59:37', '2021-09-21 17:59:37', '', 'Вступ на 5-й курс', '', 'publish', 'closed', 'closed', '', '%d0%b2%d1%81%d1%82%d1%83%d0%bf-%d0%bd%d0%b0-5-%d0%b9-%d0%ba%d1%83%d1%80%d1%81', '', '', '2021-09-21 20:59:37', '2021-09-21 17:59:37', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=43', 0, 'page', '', 0),
(44, 1, '2021-09-21 20:59:37', '2021-09-21 17:59:37', '', 'Вступ на 5-й курс', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2021-09-21 20:59:37', '2021-09-21 17:59:37', '', 43, 'http://localhost/departmentsite/wordpress/?p=44', 0, 'revision', '', 0),
(45, 1, '2021-09-21 20:59:49', '2021-09-21 17:59:49', '', 'Аспірантура', '', 'publish', 'closed', 'closed', '', '%d0%b0%d1%81%d0%bf%d1%96%d1%80%d0%b0%d0%bd%d1%82%d1%83%d1%80%d0%b0', '', '', '2021-09-21 20:59:49', '2021-09-21 17:59:49', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=45', 0, 'page', '', 0),
(46, 1, '2021-09-21 20:59:49', '2021-09-21 17:59:49', '', 'Аспірантура', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2021-09-21 20:59:49', '2021-09-21 17:59:49', '', 45, 'http://localhost/departmentsite/wordpress/?p=46', 0, 'revision', '', 0),
(47, 1, '2021-09-21 21:00:06', '2021-09-21 18:00:06', '', 'Друга освіта', '', 'publish', 'closed', 'closed', '', '%d0%b4%d1%80%d1%83%d0%b3%d0%b0-%d0%be%d1%81%d0%b2%d1%96%d1%82%d0%b0', '', '', '2021-09-21 21:00:06', '2021-09-21 18:00:06', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=47', 0, 'page', '', 0),
(48, 1, '2021-09-21 21:00:06', '2021-09-21 18:00:06', '', 'Друга освіта', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2021-09-21 21:00:06', '2021-09-21 18:00:06', '', 47, 'http://localhost/departmentsite/wordpress/?p=48', 0, 'revision', '', 0),
(49, 1, '2021-09-21 21:00:15', '2021-09-21 18:00:15', '', 'Спеціальності та освітні програми', '', 'publish', 'closed', 'closed', '', '%d1%81%d0%bf%d0%b5%d1%86%d1%96%d0%b0%d0%bb%d1%8c%d0%bd%d0%be%d1%81%d1%82%d1%96-%d1%82%d0%b0-%d0%be%d1%81%d0%b2%d1%96%d1%82%d0%bd%d1%96-%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%b8', '', '', '2021-09-21 21:00:15', '2021-09-21 18:00:15', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=49', 0, 'page', '', 0),
(50, 1, '2021-09-21 21:00:15', '2021-09-21 18:00:15', '', 'Спеціальності та освітні програми', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2021-09-21 21:00:15', '2021-09-21 18:00:15', '', 49, 'http://localhost/departmentsite/wordpress/?p=50', 0, 'revision', '', 0),
(51, 1, '2021-09-21 21:00:26', '2021-09-21 18:00:26', '', 'Навчальна документація', '', 'publish', 'closed', 'closed', '', '%d0%bd%d0%b0%d0%b2%d1%87%d0%b0%d0%bb%d1%8c%d0%bd%d0%b0-%d0%b4%d0%be%d0%ba%d1%83%d0%bc%d0%b5%d0%bd%d1%82%d0%b0%d1%86%d1%96%d1%8f', '', '', '2021-09-21 21:00:26', '2021-09-21 18:00:26', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=51', 0, 'page', '', 0),
(52, 1, '2021-09-21 21:00:26', '2021-09-21 18:00:26', '', 'Навчальна документація', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2021-09-21 21:00:26', '2021-09-21 18:00:26', '', 51, 'http://localhost/departmentsite/wordpress/?p=52', 0, 'revision', '', 0),
(53, 1, '2021-09-21 21:00:36', '2021-09-21 18:00:36', '', 'Поточні зміни розкладу', '', 'publish', 'closed', 'closed', '', '%d0%bf%d0%be%d1%82%d0%be%d1%87%d0%bd%d1%96-%d0%b7%d0%bc%d1%96%d0%bd%d0%b8-%d1%80%d0%be%d0%b7%d0%ba%d0%bb%d0%b0%d0%b4%d1%83', '', '', '2021-09-21 21:00:36', '2021-09-21 18:00:36', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=53', 0, 'page', '', 0),
(54, 1, '2021-09-21 21:00:36', '2021-09-21 18:00:36', '', 'Поточні зміни розкладу', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2021-09-21 21:00:36', '2021-09-21 18:00:36', '', 53, 'http://localhost/departmentsite/wordpress/?p=54', 0, 'revision', '', 0),
(55, 1, '2021-09-21 21:00:48', '2021-09-21 18:00:48', '', 'Контакти кураторів груп', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8-%d0%ba%d1%83%d1%80%d0%b0%d1%82%d0%be%d1%80%d1%96%d0%b2-%d0%b3%d1%80%d1%83%d0%bf', '', '', '2021-09-21 21:00:48', '2021-09-21 18:00:48', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=55', 0, 'page', '', 0),
(56, 1, '2021-09-21 21:00:48', '2021-09-21 18:00:48', '', 'Контакти кураторів груп', '', 'inherit', 'closed', 'closed', '', '55-revision-v1', '', '', '2021-09-21 21:00:48', '2021-09-21 18:00:48', '', 55, 'http://localhost/departmentsite/wordpress/?p=56', 0, 'revision', '', 0),
(57, 1, '2021-09-21 21:01:00', '2021-09-21 18:01:00', '', 'Контакти викладачів', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8-%d0%b2%d0%b8%d0%ba%d0%bb%d0%b0%d0%b4%d0%b0%d1%87%d1%96%d0%b2', '', '', '2021-09-21 21:01:00', '2021-09-21 18:01:00', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=57', 0, 'page', '', 0),
(58, 1, '2021-09-21 21:01:00', '2021-09-21 18:01:00', '', 'Контакти викладачів', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2021-09-21 21:01:00', '2021-09-21 18:01:00', '', 57, 'http://localhost/departmentsite/wordpress/?p=58', 0, 'revision', '', 0),
(59, 1, '2021-09-21 21:01:11', '2021-09-21 18:01:11', '', 'Дистанційні ресурси', '', 'publish', 'closed', 'closed', '', '%d0%b4%d0%b8%d1%81%d1%82%d0%b0%d0%bd%d1%86%d1%96%d0%b9%d0%bd%d1%96-%d1%80%d0%b5%d1%81%d1%83%d1%80%d1%81%d0%b8', '', '', '2021-09-21 21:01:11', '2021-09-21 18:01:11', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=59', 0, 'page', '', 0),
(60, 1, '2021-09-21 21:01:11', '2021-09-21 18:01:11', '', 'Дистанційні ресурси', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2021-09-21 21:01:11', '2021-09-21 18:01:11', '', 59, 'http://localhost/departmentsite/wordpress/?p=60', 0, 'revision', '', 0),
(61, 1, '2021-09-21 21:01:50', '2021-09-21 18:01:50', '', 'Наукові напрямки', '', 'publish', 'closed', 'closed', '', '%d0%bd%d0%b0%d1%83%d0%ba%d0%be%d0%b2%d1%96-%d0%bd%d0%b0%d0%bf%d1%80%d1%8f%d0%bc%d0%ba%d0%b8', '', '', '2021-09-21 21:01:50', '2021-09-21 18:01:50', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=61', 0, 'page', '', 0),
(62, 1, '2021-09-21 21:01:50', '2021-09-21 18:01:50', '', 'Наукові напрямки', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2021-09-21 21:01:50', '2021-09-21 18:01:50', '', 61, 'http://localhost/departmentsite/wordpress/?p=62', 0, 'revision', '', 0),
(63, 1, '2021-09-21 21:02:01', '2021-09-21 18:02:01', '', 'Наукові семінари', '', 'publish', 'closed', 'closed', '', '%d0%bd%d0%b0%d1%83%d0%ba%d0%be%d0%b2%d1%96-%d1%81%d0%b5%d0%bc%d1%96%d0%bd%d0%b0%d1%80%d0%b8', '', '', '2021-09-21 21:02:01', '2021-09-21 18:02:01', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=63', 0, 'page', '', 0),
(64, 1, '2021-09-21 21:02:01', '2021-09-21 18:02:01', '', 'Наукові семінари', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2021-09-21 21:02:01', '2021-09-21 18:02:01', '', 63, 'http://localhost/departmentsite/wordpress/?p=64', 0, 'revision', '', 0),
(65, 1, '2021-09-21 21:02:11', '2021-09-21 18:02:11', '', 'Виконання наукових тем', '', 'publish', 'closed', 'closed', '', '%d0%b2%d0%b8%d0%ba%d0%be%d0%bd%d0%b0%d0%bd%d0%bd%d1%8f-%d0%bd%d0%b0%d1%83%d0%ba%d0%be%d0%b2%d0%b8%d1%85-%d1%82%d0%b5%d0%bc', '', '', '2021-09-21 21:02:11', '2021-09-21 18:02:11', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=65', 0, 'page', '', 0),
(66, 1, '2021-09-21 21:02:11', '2021-09-21 18:02:11', '', 'Виконання наукових тем', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2021-09-21 21:02:11', '2021-09-21 18:02:11', '', 65, 'http://localhost/departmentsite/wordpress/?p=66', 0, 'revision', '', 0),
(67, 1, '2021-09-21 21:02:21', '2021-09-21 18:02:21', '', 'Конференції', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%be%d0%bd%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d1%96%d1%97', '', '', '2021-09-21 21:02:21', '2021-09-21 18:02:21', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=67', 0, 'page', '', 0),
(68, 1, '2021-09-21 21:02:21', '2021-09-21 18:02:21', '', 'Конференції', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2021-09-21 21:02:21', '2021-09-21 18:02:21', '', 67, 'http://localhost/departmentsite/wordpress/?p=68', 0, 'revision', '', 0),
(69, 1, '2021-09-21 21:02:31', '2021-09-21 18:02:31', '', 'Звіти по науці', '', 'publish', 'closed', 'closed', '', '%d0%b7%d0%b2%d1%96%d1%82%d0%b8-%d0%bf%d0%be-%d0%bd%d0%b0%d1%83%d1%86%d1%96', '', '', '2021-09-21 21:02:31', '2021-09-21 18:02:31', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=69', 0, 'page', '', 0),
(70, 1, '2021-09-21 21:02:31', '2021-09-21 18:02:31', '', 'Звіти по науці', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2021-09-21 21:02:31', '2021-09-21 18:02:31', '', 69, 'http://localhost/departmentsite/wordpress/?p=70', 0, 'revision', '', 0),
(71, 1, '2021-09-21 21:02:46', '2021-09-21 18:02:46', '', 'Студентський актив', '', 'publish', 'closed', 'closed', '', '%d1%81%d1%82%d1%83%d0%b4%d0%b5%d0%bd%d1%82%d1%81%d1%8c%d0%ba%d0%b8%d0%b9-%d0%b0%d0%ba%d1%82%d0%b8%d0%b2', '', '', '2021-09-21 21:02:46', '2021-09-21 18:02:46', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=71', 0, 'page', '', 0),
(72, 1, '2021-09-21 21:02:46', '2021-09-21 18:02:46', '', 'Студентський актив', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2021-09-21 21:02:46', '2021-09-21 18:02:46', '', 71, 'http://localhost/departmentsite/wordpress/?p=72', 0, 'revision', '', 0),
(73, 1, '2021-09-21 21:02:56', '2021-09-21 18:02:56', '', 'Студентські гуртки', '', 'publish', 'closed', 'closed', '', '%d1%81%d1%82%d1%83%d0%b4%d0%b5%d0%bd%d1%82%d1%81%d1%8c%d0%ba%d1%96-%d0%b3%d1%83%d1%80%d1%82%d0%ba%d0%b8', '', '', '2021-09-21 21:02:56', '2021-09-21 18:02:56', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=73', 0, 'page', '', 0),
(74, 1, '2021-09-21 21:02:56', '2021-09-21 18:02:56', '', 'Студентські гуртки', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2021-09-21 21:02:56', '2021-09-21 18:02:56', '', 73, 'http://localhost/departmentsite/wordpress/?p=74', 0, 'revision', '', 0),
(75, 1, '2021-09-21 21:03:06', '2021-09-21 18:03:06', '', 'Студентські конференції', '', 'publish', 'closed', 'closed', '', '%d1%81%d1%82%d1%83%d0%b4%d0%b5%d0%bd%d1%82%d1%81%d1%8c%d0%ba%d1%96-%d0%ba%d0%be%d0%bd%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d1%96%d1%97', '', '', '2021-09-21 21:03:06', '2021-09-21 18:03:06', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=75', 0, 'page', '', 0),
(76, 1, '2021-09-21 21:03:06', '2021-09-21 18:03:06', '', 'Студентські конференції', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2021-09-21 21:03:06', '2021-09-21 18:03:06', '', 75, 'http://localhost/departmentsite/wordpress/?p=76', 0, 'revision', '', 0),
(77, 1, '2021-09-21 21:03:15', '2021-09-21 18:03:15', '', 'Іменні стипендії', '', 'publish', 'closed', 'closed', '', '%d1%96%d0%bc%d0%b5%d0%bd%d0%bd%d1%96-%d1%81%d1%82%d0%b8%d0%bf%d0%b5%d0%bd%d0%b4%d1%96%d1%97', '', '', '2021-09-21 21:03:15', '2021-09-21 18:03:15', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=77', 0, 'page', '', 0),
(78, 1, '2021-09-21 21:03:15', '2021-09-21 18:03:15', '', 'Іменні стипендії', '', 'inherit', 'closed', 'closed', '', '77-revision-v1', '', '', '2021-09-21 21:03:15', '2021-09-21 18:03:15', '', 77, 'http://localhost/departmentsite/wordpress/?p=78', 0, 'revision', '', 0),
(79, 1, '2021-09-21 21:03:25', '2021-09-21 18:03:25', '', 'Партнери', '', 'publish', 'closed', 'closed', '', '%d0%bf%d0%b0%d1%80%d1%82%d0%bd%d0%b5%d1%80%d0%b8-2', '', '', '2021-09-21 21:03:25', '2021-09-21 18:03:25', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=79', 0, 'page', '', 0),
(80, 1, '2021-09-21 21:03:25', '2021-09-21 18:03:25', '', 'Партнери', '', 'inherit', 'closed', 'closed', '', '79-revision-v1', '', '', '2021-09-21 21:03:25', '2021-09-21 18:03:25', '', 79, 'http://localhost/departmentsite/wordpress/?p=80', 0, 'revision', '', 0),
(81, 1, '2021-09-21 21:03:44', '2021-09-21 18:03:44', '', 'Міжнародні програми', '', 'publish', 'closed', 'closed', '', '%d0%bc%d1%96%d0%b6%d0%bd%d0%b0%d1%80%d0%be%d0%b4%d0%bd%d1%96-%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%b8', '', '', '2021-09-21 21:03:44', '2021-09-21 18:03:44', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=81', 0, 'page', '', 0),
(82, 1, '2021-09-21 21:03:44', '2021-09-21 18:03:44', '', 'Міжнародні програми', '', 'inherit', 'closed', 'closed', '', '81-revision-v1', '', '', '2021-09-21 21:03:44', '2021-09-21 18:03:44', '', 81, 'http://localhost/departmentsite/wordpress/?p=82', 0, 'revision', '', 0),
(83, 1, '2021-09-21 21:03:59', '2021-09-21 18:03:59', '', 'Теми по міжнародному співробітництву', '', 'publish', 'closed', 'closed', '', '%d1%82%d0%b5%d0%bc%d0%b8-%d0%bf%d0%be-%d0%bc%d1%96%d0%b6%d0%bd%d0%b0%d1%80%d0%be%d0%b4%d0%bd%d0%be%d0%bc%d1%83-%d1%81%d0%bf%d1%96%d0%b2%d1%80%d0%be%d0%b1%d1%96%d1%82%d0%bd%d0%b8%d1%86%d1%82%d0%b2', '', '', '2021-09-21 21:03:59', '2021-09-21 18:03:59', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=83', 0, 'page', '', 0),
(84, 1, '2021-09-21 21:03:59', '2021-09-21 18:03:59', '', 'Теми по міжнародному співробітництву', '', 'inherit', 'closed', 'closed', '', '83-revision-v1', '', '', '2021-09-21 21:03:59', '2021-09-21 18:03:59', '', 83, 'http://localhost/departmentsite/wordpress/?p=84', 0, 'revision', '', 0),
(85, 1, '2021-09-21 21:04:13', '2021-09-21 18:04:13', '', 'Новини', '', 'publish', 'closed', 'closed', '', '%d0%bd%d0%be%d0%b2%d0%b8%d0%bd%d0%b8', '', '', '2021-09-21 21:04:13', '2021-09-21 18:04:13', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=85', 0, 'page', '', 0),
(86, 1, '2021-09-21 21:04:13', '2021-09-21 18:04:13', '', 'Новини', '', 'inherit', 'closed', 'closed', '', '85-revision-v1', '', '', '2021-09-21 21:04:13', '2021-09-21 18:04:13', '', 85, 'http://localhost/departmentsite/wordpress/?p=86', 0, 'revision', '', 0),
(87, 1, '2021-09-21 21:04:23', '2021-09-21 18:04:23', '', 'Оголошення', '', 'publish', 'closed', 'closed', '', '%d0%be%d0%b3%d0%be%d0%bb%d0%be%d1%88%d0%b5%d0%bd%d0%bd%d1%8f', '', '', '2021-09-21 21:04:23', '2021-09-21 18:04:23', '', 0, 'http://localhost/departmentsite/wordpress/?page_id=87', 0, 'page', '', 0),
(88, 1, '2021-09-21 21:04:23', '2021-09-21 18:04:23', '', 'Оголошення', '', 'inherit', 'closed', 'closed', '', '87-revision-v1', '', '', '2021-09-21 21:04:23', '2021-09-21 18:04:23', '', 87, 'http://localhost/departmentsite/wordpress/?p=88', 0, 'revision', '', 0),
(89, 1, '2021-09-21 21:16:22', '2021-09-21 18:16:22', ' ', '', '', 'publish', 'closed', 'closed', '', '89', '', '', '2021-09-21 21:16:22', '2021-09-21 18:16:22', '', 0, 'http://localhost/departmentsite/wordpress/?p=89', 10, 'nav_menu_item', '', 0),
(90, 1, '2021-09-21 21:16:20', '2021-09-21 18:16:20', ' ', '', '', 'publish', 'closed', 'closed', '', '90', '', '', '2021-09-21 21:16:20', '2021-09-21 18:16:20', '', 0, 'http://localhost/departmentsite/wordpress/?p=90', 1, 'nav_menu_item', '', 0),
(91, 1, '2021-09-21 21:16:27', '2021-09-21 18:16:27', ' ', '', '', 'publish', 'closed', 'closed', '', '91', '', '', '2021-09-21 21:16:27', '2021-09-21 18:16:27', '', 0, 'http://localhost/departmentsite/wordpress/?p=91', 34, 'nav_menu_item', '', 0),
(92, 1, '2021-09-21 21:16:23', '2021-09-21 18:16:23', ' ', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2021-09-21 21:16:23', '2021-09-21 18:16:23', '', 0, 'http://localhost/departmentsite/wordpress/?p=92', 15, 'nav_menu_item', '', 0),
(93, 1, '2021-09-21 21:16:25', '2021-09-21 18:16:25', ' ', '', '', 'publish', 'closed', 'closed', '', '93', '', '', '2021-09-21 21:16:25', '2021-09-21 18:16:25', '', 0, 'http://localhost/departmentsite/wordpress/?p=93', 22, 'nav_menu_item', '', 0),
(94, 1, '2021-09-21 21:16:28', '2021-09-21 18:16:28', ' ', '', '', 'publish', 'closed', 'closed', '', '94', '', '', '2021-09-21 21:16:28', '2021-09-21 18:16:28', '', 0, 'http://localhost/departmentsite/wordpress/?p=94', 38, 'nav_menu_item', '', 0),
(95, 1, '2021-09-21 21:16:26', '2021-09-21 18:16:26', ' ', '', '', 'publish', 'closed', 'closed', '', '95', '', '', '2021-09-21 21:16:26', '2021-09-21 18:16:26', '', 0, 'http://localhost/departmentsite/wordpress/?p=95', 28, 'nav_menu_item', '', 0),
(96, 1, '2021-09-21 21:16:27', '2021-09-21 18:16:27', ' ', '', '', 'publish', 'closed', 'closed', '', '96', '', '', '2021-09-21 21:16:27', '2021-09-21 18:16:27', '', 0, 'http://localhost/departmentsite/wordpress/?p=96', 29, 'nav_menu_item', '', 0),
(97, 1, '2021-09-21 21:16:21', '2021-09-21 18:16:21', ' ', '', '', 'publish', 'closed', 'closed', '', '97', '', '', '2021-09-21 21:16:21', '2021-09-21 18:16:21', '', 0, 'http://localhost/departmentsite/wordpress/?p=97', 7, 'nav_menu_item', '', 0),
(98, 1, '2021-09-21 21:16:20', '2021-09-21 18:16:20', ' ', '', '', 'publish', 'closed', 'closed', '', '98', '', '', '2021-09-21 21:16:20', '2021-09-21 18:16:20', '', 0, 'http://localhost/departmentsite/wordpress/?p=98', 2, 'nav_menu_item', '', 0),
(99, 1, '2021-09-21 21:16:20', '2021-09-21 18:16:20', ' ', '', '', 'publish', 'closed', 'closed', '', '99', '', '', '2021-09-21 21:16:20', '2021-09-21 18:16:20', '', 0, 'http://localhost/departmentsite/wordpress/?p=99', 3, 'nav_menu_item', '', 0),
(100, 1, '2021-09-21 21:16:22', '2021-09-21 18:16:22', ' ', '', '', 'publish', 'closed', 'closed', '', '100', '', '', '2021-09-21 21:16:22', '2021-09-21 18:16:22', '', 0, 'http://localhost/departmentsite/wordpress/?p=100', 8, 'nav_menu_item', '', 0),
(101, 1, '2021-09-21 21:16:21', '2021-09-21 18:16:21', ' ', '', '', 'publish', 'closed', 'closed', '', '101', '', '', '2021-09-21 21:16:21', '2021-09-21 18:16:21', '', 0, 'http://localhost/departmentsite/wordpress/?p=101', 6, 'nav_menu_item', '', 0),
(102, 1, '2021-09-21 21:16:21', '2021-09-21 18:16:21', ' ', '', '', 'publish', 'closed', 'closed', '', '102', '', '', '2021-09-21 21:16:21', '2021-09-21 18:16:21', '', 0, 'http://localhost/departmentsite/wordpress/?p=102', 4, 'nav_menu_item', '', 0),
(103, 1, '2021-09-21 21:16:22', '2021-09-21 18:16:22', ' ', '', '', 'publish', 'closed', 'closed', '', '103', '', '', '2021-09-21 21:16:22', '2021-09-21 18:16:22', '', 0, 'http://localhost/departmentsite/wordpress/?p=103', 9, 'nav_menu_item', '', 0),
(104, 1, '2021-09-21 21:16:21', '2021-09-21 18:16:21', ' ', '', '', 'publish', 'closed', 'closed', '', '104', '', '', '2021-09-21 21:16:21', '2021-09-21 18:16:21', '', 0, 'http://localhost/departmentsite/wordpress/?p=104', 5, 'nav_menu_item', '', 0),
(105, 1, '2021-09-21 21:16:23', '2021-09-21 18:16:23', ' ', '', '', 'publish', 'closed', 'closed', '', '105', '', '', '2021-09-21 21:16:23', '2021-09-21 18:16:23', '', 0, 'http://localhost/departmentsite/wordpress/?p=105', 13, 'nav_menu_item', '', 0),
(106, 1, '2021-09-21 21:16:22', '2021-09-21 18:16:22', ' ', '', '', 'publish', 'closed', 'closed', '', '106', '', '', '2021-09-21 21:16:22', '2021-09-21 18:16:22', '', 0, 'http://localhost/departmentsite/wordpress/?p=106', 12, 'nav_menu_item', '', 0),
(107, 1, '2021-09-21 21:16:22', '2021-09-21 18:16:22', ' ', '', '', 'publish', 'closed', 'closed', '', '107', '', '', '2021-09-21 21:16:22', '2021-09-21 18:16:22', '', 0, 'http://localhost/departmentsite/wordpress/?p=107', 11, 'nav_menu_item', '', 0),
(108, 1, '2021-09-21 21:16:23', '2021-09-21 18:16:23', ' ', '', '', 'publish', 'closed', 'closed', '', '108', '', '', '2021-09-21 21:16:23', '2021-09-21 18:16:23', '', 0, 'http://localhost/departmentsite/wordpress/?p=108', 14, 'nav_menu_item', '', 0),
(109, 1, '2021-09-21 21:16:25', '2021-09-21 18:16:25', ' ', '', '', 'publish', 'closed', 'closed', '', '109', '', '', '2021-09-21 21:16:25', '2021-09-21 18:16:25', '', 0, 'http://localhost/departmentsite/wordpress/?p=109', 21, 'nav_menu_item', '', 0),
(110, 1, '2021-09-21 21:16:24', '2021-09-21 18:16:24', ' ', '', '', 'publish', 'closed', 'closed', '', '110', '', '', '2021-09-21 21:16:24', '2021-09-21 18:16:24', '', 0, 'http://localhost/departmentsite/wordpress/?p=110', 20, 'nav_menu_item', '', 0),
(111, 1, '2021-09-21 21:16:24', '2021-09-21 18:16:24', ' ', '', '', 'publish', 'closed', 'closed', '', '111', '', '', '2021-09-21 21:16:24', '2021-09-21 18:16:24', '', 0, 'http://localhost/departmentsite/wordpress/?p=111', 19, 'nav_menu_item', '', 0),
(112, 1, '2021-09-21 21:16:23', '2021-09-21 18:16:23', ' ', '', '', 'publish', 'closed', 'closed', '', '112', '', '', '2021-09-21 21:16:23', '2021-09-21 18:16:23', '', 0, 'http://localhost/departmentsite/wordpress/?p=112', 17, 'nav_menu_item', '', 0),
(113, 1, '2021-09-21 21:16:23', '2021-09-21 18:16:23', ' ', '', '', 'publish', 'closed', 'closed', '', '113', '', '', '2021-09-21 21:16:23', '2021-09-21 18:16:23', '', 0, 'http://localhost/departmentsite/wordpress/?p=113', 18, 'nav_menu_item', '', 0),
(114, 1, '2021-09-21 21:16:23', '2021-09-21 18:16:23', ' ', '', '', 'publish', 'closed', 'closed', '', '114', '', '', '2021-09-21 21:16:23', '2021-09-21 18:16:23', '', 0, 'http://localhost/departmentsite/wordpress/?p=114', 16, 'nav_menu_item', '', 0),
(115, 1, '2021-09-21 21:16:26', '2021-09-21 18:16:26', ' ', '', '', 'publish', 'closed', 'closed', '', '115', '', '', '2021-09-21 21:16:26', '2021-09-21 18:16:26', '', 0, 'http://localhost/departmentsite/wordpress/?p=115', 25, 'nav_menu_item', '', 0),
(116, 1, '2021-09-21 21:16:26', '2021-09-21 18:16:26', ' ', '', '', 'publish', 'closed', 'closed', '', '116', '', '', '2021-09-21 21:16:26', '2021-09-21 18:16:26', '', 0, 'http://localhost/departmentsite/wordpress/?p=116', 27, 'nav_menu_item', '', 0),
(117, 1, '2021-09-21 21:16:26', '2021-09-21 18:16:26', ' ', '', '', 'publish', 'closed', 'closed', '', '117', '', '', '2021-09-21 21:16:26', '2021-09-21 18:16:26', '', 0, 'http://localhost/departmentsite/wordpress/?p=117', 26, 'nav_menu_item', '', 0),
(118, 1, '2021-09-21 21:16:26', '2021-09-21 18:16:26', ' ', '', '', 'publish', 'closed', 'closed', '', '118', '', '', '2021-09-21 21:16:26', '2021-09-21 18:16:26', '', 0, 'http://localhost/departmentsite/wordpress/?p=118', 24, 'nav_menu_item', '', 0),
(119, 1, '2021-09-21 21:16:25', '2021-09-21 18:16:25', ' ', '', '', 'publish', 'closed', 'closed', '', '119', '', '', '2021-09-21 21:16:25', '2021-09-21 18:16:25', '', 0, 'http://localhost/departmentsite/wordpress/?p=119', 23, 'nav_menu_item', '', 0),
(120, 1, '2021-09-21 21:16:27', '2021-09-21 18:16:27', ' ', '', '', 'publish', 'closed', 'closed', '', '120', '', '', '2021-09-21 21:16:27', '2021-09-21 18:16:27', '', 0, 'http://localhost/departmentsite/wordpress/?p=120', 33, 'nav_menu_item', '', 0),
(121, 1, '2021-09-21 21:16:27', '2021-09-21 18:16:27', ' ', '', '', 'publish', 'closed', 'closed', '', '121', '', '', '2021-09-21 21:16:27', '2021-09-21 18:16:27', '', 0, 'http://localhost/departmentsite/wordpress/?p=121', 30, 'nav_menu_item', '', 0),
(122, 1, '2021-09-21 21:16:27', '2021-09-21 18:16:27', ' ', '', '', 'publish', 'closed', 'closed', '', '122', '', '', '2021-09-21 21:16:27', '2021-09-21 18:16:27', '', 0, 'http://localhost/departmentsite/wordpress/?p=122', 31, 'nav_menu_item', '', 0),
(123, 1, '2021-09-21 21:16:27', '2021-09-21 18:16:27', ' ', '', '', 'publish', 'closed', 'closed', '', '123', '', '', '2021-09-21 21:16:27', '2021-09-21 18:16:27', '', 0, 'http://localhost/departmentsite/wordpress/?p=123', 32, 'nav_menu_item', '', 0),
(124, 1, '2021-09-21 21:16:28', '2021-09-21 18:16:28', ' ', '', '', 'publish', 'closed', 'closed', '', '124', '', '', '2021-09-21 21:16:28', '2021-09-21 18:16:28', '', 0, 'http://localhost/departmentsite/wordpress/?p=124', 36, 'nav_menu_item', '', 0),
(125, 1, '2021-09-21 21:16:27', '2021-09-21 18:16:27', ' ', '', '', 'publish', 'closed', 'closed', '', '125', '', '', '2021-09-21 21:16:27', '2021-09-21 18:16:27', '', 0, 'http://localhost/departmentsite/wordpress/?p=125', 35, 'nav_menu_item', '', 0),
(126, 1, '2021-09-21 21:16:28', '2021-09-21 18:16:28', ' ', '', '', 'publish', 'closed', 'closed', '', '126', '', '', '2021-09-21 21:16:28', '2021-09-21 18:16:28', '', 0, 'http://localhost/departmentsite/wordpress/?p=126', 37, 'nav_menu_item', '', 0),
(127, 1, '2021-09-21 21:16:29', '2021-09-21 18:16:29', ' ', '', '', 'publish', 'closed', 'closed', '', '127', '', '', '2021-09-21 21:16:29', '2021-09-21 18:16:29', '', 0, 'http://localhost/departmentsite/wordpress/?p=127', 39, 'nav_menu_item', '', 0),
(128, 1, '2021-09-21 21:16:29', '2021-09-21 18:16:29', ' ', '', '', 'publish', 'closed', 'closed', '', '128', '', '', '2021-09-21 21:16:29', '2021-09-21 18:16:29', '', 0, 'http://localhost/departmentsite/wordpress/?p=128', 40, 'nav_menu_item', '', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Без категорії', '%d0%b1%d0%b5%d0%b7-%d0%ba%d0%b0%d1%82%d0%b5%d0%b3%d0%be%d1%80%d1%96%d1%97', 0),
(2, 'Головне меню', '%d0%b3%d0%be%d0%bb%d0%be%d0%b2%d0%bd%d0%b5-%d0%bc%d0%b5%d0%bd%d1%8e', 0),
(3, 'Загальна інформація про підрозділ', '%d0%b7%d0%b0%d0%b3%d0%b0%d0%bb%d1%8c%d0%bd%d0%b0-%d1%96%d0%bd%d1%84%d0%be%d1%80%d0%bc%d0%b0%d1%86%d1%96%d1%8f-%d0%bf%d1%80%d0%be-%d0%bf%d1%96%d0%b4%d1%80%d0%be%d0%b7%d0%b4%d1%96%d0%bb', 0),
(4, 'Вступ', '%d0%b2%d1%81%d1%82%d1%83%d0%bf', 0),
(5, 'Навчання', '%d0%bd%d0%b0%d0%b2%d1%87%d0%b0%d0%bd%d0%bd%d1%8f', 0),
(6, 'Наука', '%d0%bd%d0%b0%d1%83%d0%ba%d0%b0', 0),
(7, 'Персонал', '%d0%bf%d0%b5%d1%80%d1%81%d0%be%d0%bd%d0%b0%d0%bb', 0),
(8, 'Студентське життя', '%d1%81%d1%82%d1%83%d0%b4%d0%b5%d0%bd%d1%82%d1%81%d1%8c%d0%ba%d0%b5-%d0%b6%d0%b8%d1%82%d1%82%d1%8f', 0),
(9, 'Зарубіжне партнерство', '%d0%b7%d0%b0%d1%80%d1%83%d0%b1%d1%96%d0%b6%d0%bd%d0%b5-%d0%bf%d0%b0%d1%80%d1%82%d0%bd%d0%b5%d1%80%d1%81%d1%82%d0%b2%d0%be', 0),
(10, 'Новини/Оголошення', '%d0%bd%d0%be%d0%b2%d0%b8%d0%bd%d0%b8-%d0%be%d0%b3%d0%be%d0%bb%d0%be%d1%88%d0%b5%d0%bd%d0%bd%d1%8f', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(89, 2, 0),
(90, 2, 0),
(91, 2, 0),
(92, 2, 0),
(93, 2, 0),
(94, 2, 0),
(95, 2, 0),
(96, 2, 0),
(97, 2, 0),
(98, 2, 0),
(99, 2, 0),
(100, 2, 0),
(101, 2, 0),
(102, 2, 0),
(103, 2, 0),
(104, 2, 0),
(105, 2, 0),
(106, 2, 0),
(107, 2, 0),
(108, 2, 0),
(109, 2, 0),
(110, 2, 0),
(111, 2, 0),
(112, 2, 0),
(113, 2, 0),
(114, 2, 0),
(115, 2, 0),
(116, 2, 0),
(117, 2, 0),
(118, 2, 0),
(119, 2, 0),
(120, 2, 0),
(121, 2, 0),
(122, 2, 0),
(123, 2, 0),
(124, 2, 0),
(125, 2, 0),
(126, 2, 0),
(127, 2, 0),
(128, 2, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 40),
(3, 3, 'category', '', 0, 0),
(4, 4, 'category', '', 0, 0),
(5, 5, 'category', '', 0, 0),
(6, 6, 'category', '', 0, 0),
(7, 7, 'category', '', 0, 0),
(8, 8, 'category', '', 0, 0),
(9, 9, 'category', '', 0, 0),
(10, 10, 'category', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'Dev'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"966a2be5b11e1c29a6d4c7274a61dbde2f61a8df477822f91b876b256ccf5bd6\";a:4:{s:10:\"expiration\";i:1632419160;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36\";s:5:\"login\";i:1632246360;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '24'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:18:\"mega_menu_meta_box\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";}'),
(20, 1, 'nav_menu_recently_edited', '2'),
(21, 1, 'wp_user-settings', 'libraryContent=browse'),
(22, 1, 'wp_user-settings-time', '1632246358');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'Dev', '$P$BvuWBAxpp9eGznY1ZCGomSkXUgXAxI.', 'dev', 'poanan96@gmail.com', 'http://localhost/departmentsite/wordpress', '2021-07-23 17:08:04', '', 0, 'Dev');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Індекси таблиці `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Індекси таблиці `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Індекси таблиці `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Індекси таблиці `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Індекси таблиці `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Індекси таблиці `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Індекси таблиці `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=397;

--
-- AUTO_INCREMENT для таблиці `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=441;

--
-- AUTO_INCREMENT для таблиці `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT для таблиці `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблиці `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблиці `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблиці `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
